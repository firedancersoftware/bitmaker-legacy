///-----------------------------------------------------------------
///
/// @file      BitMakerFrm.cpp
/// @author    Firedancer Software
/// Created:   21/05/2012 8:51:27 AM
/// @section   DESCRIPTION
///            BitMakerFrm class implementation
///
///------------------------------------------------------------------

#include "BitMakerFrm.h"

//Do not add custom headers between
//Header Include Start and Header Include End
//wxDev-C++ designer will remove them
////Header Include Start
////Header Include End

#include "Objects/MingW/BitMaker_private.h"
#include "CheckForUpdatesThread.h"
#include <wx/textfile.h>
#include <wx/file.h>
#include <wx/filefn.h>
#include <wx/filename.h>
#include <wx/aboutdlg.h>
#include <wx/richmsgdlg.h>
#include <wx/valnum.h>
#include <wx/fileconf.h>
#include <wx/wfstream.h>

//----------------------------------------------------------------------------
// BitMakerFrm
//----------------------------------------------------------------------------
//Add Custom Events only in the appropriate block.
//Code added in other places will be removed by wxDev-C++
////Event Table Start
BEGIN_EVENT_TABLE(BitMakerFrm,wxFrame)
	////Manual Code Start
	EVT_COMMAND(wxID_ANY, wxEVT_CHECKFORUPDATESTHREAD, BitMakerFrm::OnCheckUpdateThread)
	EVT_MENU(ID_MNU_LOADLASTPROFILE, BitMakerFrm::MnuLoadProfileOnStartupClick)
	EVT_MENU_RANGE(wxID_FILE1, wxID_FILE9, BitMakerFrm::OnRecentProfileClick)
	////Manual Code End

	EVT_CLOSE(BitMakerFrm::OnClose)
	EVT_MENU(ID_MNU_LOADPROFILE_1195, BitMakerFrm::Mnuloadprofile1195Click)
	EVT_MENU(ID_MNU_UNLOADPROFILE_1198, BitMakerFrm::Mnuunloadprofile1198Click)
	EVT_MENU(ID_MNU_EXIT_1009, BitMakerFrm::Mnuexit1009Click)
	EVT_MENU(ID_MNU_CHECKFORUPDATES_1203, BitMakerFrm::Mnucheckforupdates1203Click)
	EVT_MENU(ID_MNU_WEBSITE_1007, BitMakerFrm::Mnuwebsite1007Click)
	EVT_MENU(ID_MNU_ABOUT_1008, BitMakerFrm::Mnuabout1008Click)
	EVT_BUTTON(ID_WXEXITBUTTON,BitMakerFrm::WxExitButtonClick)
	EVT_BUTTON(ID_WXALLOFFBUTTON,BitMakerFrm::WxAllOffButtonClick)
	EVT_BUTTON(ID_WXALLONBUTTON,BitMakerFrm::WxAllOnButtonClick)

	EVT_TEXT(ID_WXDECIMALEDIT,BitMakerFrm::WxCalcButtonClick)
END_EVENT_TABLE()
////Event Table End

BitMakerFrm::BitMakerFrm(wxString defaultProfilePath, wxString defaultConfigPath, wxWindow *parent, wxWindowID id, const wxString &title, const wxPoint &position, const wxSize& size, long style)
: wxFrame(parent, id, title, position, size, style)
{
	profilePath = defaultProfilePath;
	configPath = defaultConfigPath;

	CreateGUIControls();

	bIsCheckingUpdates = false;
	CheckForUpdates(true);

	normalFont = WxBit0->GetFont();
	boldFont = WxBit0->GetFont();
	boldFont.SetWeight(wxFONTWEIGHT_BOLD);


	wxString configFile;
	configFile << configPath << wxFileName::GetPathSeparator() << wxT("BitMaker.ini");

	if(!wxFileExists(configFile))
	{
		wxFile* tempFile = new wxFile();
		tempFile->Create(configFile);
	}
	m_fileConfig = new wxFileConfig(wxT("BitMaker"), wxT("Firedancer Software"), configFile);

	wxConfigBase::Set(m_fileConfig);

	m_fileConfig->SetPath(wxT("/RecentFiles"));
	m_fileHistory->Load(*m_fileConfig);


	m_fileConfig->SetPath(wxT("/Settings"));

	bool bLoadLastProfile = false;
	m_fileConfig->Read("LoadLastProfile", &bLoadLastProfile);

	WxMenuBar1->Check(ID_MNU_LOADLASTPROFILE, bLoadLastProfile);

	m_fileConfig->SetPath("/Settings");

	wxString lastProfile = "";
	m_fileConfig->Read(wxT("LastUsedProfile"), &lastProfile);

	if(bLoadLastProfile && lastProfile != "")
	{
		loadProfile(lastProfile, true);
	}

	m_fileConfig->SetPath(wxT("/"));
}


BitMakerFrm::~BitMakerFrm()
{
}

void BitMakerFrm::CreateGUIControls()
{
	//Do not add custom code between
	//GUI Items Creation Start and GUI Items Creation End
	//wxDev-C++ designer will remove them.
	//Add the custom code before or after the blocks
	////GUI Items Creation Start

	WxBoxSizer1 = new wxBoxSizer(wxVERTICAL);
	this->SetSizer(WxBoxSizer1);
	this->SetAutoLayout(true);

	WxPanel1 = new wxPanel(this, ID_WXPANEL1, wxPoint(21, 5), wxSize(537, 342));
	WxBoxSizer1->Add(WxPanel1, 1, wxALIGN_CENTER | wxEXPAND | wxALL, 5);

	WxGridSizer2 = new wxGridSizer(0, 4, 0, 0);
	WxPanel1->SetSizer(WxGridSizer2);
	WxPanel1->SetAutoLayout(true);

	WxPanel8 = new wxPanel(WxPanel1, ID_WXPANEL8, wxPoint(5, 5), wxSize(123, 32));
	WxGridSizer2->Add(WxPanel8, 0, wxALIGN_CENTER | wxEXPAND | wxALL, 5);

	WxBoxSizer6 = new wxBoxSizer(wxHORIZONTAL);
	WxPanel8->SetSizer(WxBoxSizer6);
	WxPanel8->SetAutoLayout(true);

	WxBit0 = new wxTextCtrl(WxPanel8, ID_WXBIT0, _("0"), wxPoint(5, 5), wxSize(35, 22), wxTE_READONLY | wxTE_CENTRE, wxDefaultValidator, _("WxBit0"));
	WxBit0->SetToolTip(_("1"));
	WxBit0->SetBackgroundColour(wxColour(_("WHITE")));
	WxBoxSizer6->Add(WxBit0, 0, wxALIGN_CENTER | wxALL, 5);

	WxPanel7 = new wxPanel(WxPanel8, ID_WXPANEL7, wxPoint(45, 1), wxSize(68, 30));
	WxBoxSizer6->Add(WxPanel7, 1, wxALIGN_CENTER | 0, 0);

	WxBoxSizer39 = new wxBoxSizer(wxVERTICAL);
	WxPanel7->SetSizer(WxBoxSizer39);
	WxPanel7->SetAutoLayout(true);

	WxBit0Label = new wxStaticText(WxPanel7, ID_WXBIT0LABEL, _("1 << 0"), wxPoint(5, 5), wxSize(60, 19), wxALIGN_LEFT | wxST_NO_AUTORESIZE, _("WxBit0Label"));
	WxBit0Label->SetToolTip(_("1"));
	WxBoxSizer39->Add(WxBit0Label, 1, wxALIGN_CENTER | wxEXPAND | wxALL, 5);

	WxPanel9 = new wxPanel(WxPanel1, ID_WXPANEL9, wxPoint(138, 5), wxSize(123, 32));
	WxGridSizer2->Add(WxPanel9, 0, wxALIGN_CENTER | wxEXPAND | wxALL, 5);

	WxBoxSizer7 = new wxBoxSizer(wxHORIZONTAL);
	WxPanel9->SetSizer(WxBoxSizer7);
	WxPanel9->SetAutoLayout(true);

	WxBit8 = new wxTextCtrl(WxPanel9, ID_WXBIT8, _("0"), wxPoint(5, 5), wxSize(35, 22), wxTE_READONLY | wxTE_CENTRE, wxDefaultValidator, _("WxBit8"));
	WxBit8->SetToolTip(_("256"));
	WxBit8->SetBackgroundColour(wxColour(_("WHITE")));
	WxBoxSizer7->Add(WxBit8, 0, wxALIGN_CENTER | wxALL, 5);

	WxPanel40 = new wxPanel(WxPanel9, ID_WXPANEL40, wxPoint(45, 1), wxSize(68, 30));
	WxBoxSizer7->Add(WxPanel40, 1, wxALIGN_CENTER | 0, 0);

	WxBoxSizer40 = new wxBoxSizer(wxVERTICAL);
	WxPanel40->SetSizer(WxBoxSizer40);
	WxPanel40->SetAutoLayout(true);

	WxBit8Label = new wxStaticText(WxPanel40, ID_WXBIT8LABEL, _("1 << 8"), wxPoint(5, 5), wxSize(60, 20), wxALIGN_LEFT | wxST_NO_AUTORESIZE, _("WxBit8Label"));
	WxBit8Label->SetToolTip(_("256"));
	WxBoxSizer40->Add(WxBit8Label, 1, wxALIGN_CENTER | wxALIGN_CENTER_VERTICAL | wxEXPAND | wxALL, 5);

	WxPanel10 = new wxPanel(WxPanel1, ID_WXPANEL10, wxPoint(271, 5), wxSize(123, 32));
	WxGridSizer2->Add(WxPanel10, 0, wxALIGN_CENTER | wxEXPAND | wxALL, 5);

	WxBoxSizer8 = new wxBoxSizer(wxHORIZONTAL);
	WxPanel10->SetSizer(WxBoxSizer8);
	WxPanel10->SetAutoLayout(true);

	WxBit16 = new wxTextCtrl(WxPanel10, ID_WXBIT16, _("0"), wxPoint(5, 5), wxSize(35, 22), wxTE_READONLY | wxTE_CENTRE, wxDefaultValidator, _("WxBit16"));
	WxBit16->SetToolTip(_("65536"));
	WxBit16->SetBackgroundColour(wxColour(_("WHITE")));
	WxBoxSizer8->Add(WxBit16, 0, wxALIGN_CENTER | wxALL, 5);

	WxPanel41 = new wxPanel(WxPanel10, ID_WXPANEL41, wxPoint(45, 1), wxSize(68, 30));
	WxBoxSizer8->Add(WxPanel41, 1, wxALIGN_CENTER | 0, 0);

	WxBoxSizer41 = new wxBoxSizer(wxVERTICAL);
	WxPanel41->SetSizer(WxBoxSizer41);
	WxPanel41->SetAutoLayout(true);

	WxBit16Label = new wxStaticText(WxPanel41, ID_WXBIT16LABEL, _("1 << 16"), wxPoint(5, 5), wxSize(60, 19), wxALIGN_LEFT | wxST_NO_AUTORESIZE, _("WxBit16Label"));
	WxBit16Label->SetToolTip(_("65536"));
	WxBoxSizer41->Add(WxBit16Label, 1, wxALIGN_CENTER | wxEXPAND | wxALL, 5);

	WxPanel11 = new wxPanel(WxPanel1, ID_WXPANEL11, wxPoint(404, 5), wxSize(123, 32));
	WxGridSizer2->Add(WxPanel11, 0, wxALIGN_CENTER | wxEXPAND | wxALL, 5);

	WxBoxSizer9 = new wxBoxSizer(wxHORIZONTAL);
	WxPanel11->SetSizer(WxBoxSizer9);
	WxPanel11->SetAutoLayout(true);

	WxBit24 = new wxTextCtrl(WxPanel11, ID_WXBIT24, _("0"), wxPoint(5, 5), wxSize(36, 22), wxTE_READONLY | wxTE_CENTRE, wxDefaultValidator, _("WxBit24"));
	WxBit24->SetToolTip(_("16777216"));
	WxBit24->SetBackgroundColour(wxColour(_("WHITE")));
	WxBoxSizer9->Add(WxBit24, 0, wxALIGN_CENTER | wxALL, 5);

	WxPanel3 = new wxPanel(WxPanel11, ID_WXPANEL3, wxPoint(46, 1), wxSize(68, 30));
	WxBoxSizer9->Add(WxPanel3, 1, wxALIGN_CENTER | 0, 0);

	WxBoxSizer38 = new wxBoxSizer(wxVERTICAL);
	WxPanel3->SetSizer(WxBoxSizer38);
	WxPanel3->SetAutoLayout(true);

	WxBit24Label = new wxStaticText(WxPanel3, ID_WXBIT24LABEL, _("1 << 24"), wxPoint(5, 5), wxSize(60, 19), wxALIGN_LEFT | wxST_NO_AUTORESIZE, _("WxBit24Label"));
	WxBit24Label->SetToolTip(_("16777216"));
	WxBoxSizer38->Add(WxBit24Label, 1, wxALIGN_CENTER_VERTICAL | wxEXPAND | wxALL, 5);

	WxPanel12 = new wxPanel(WxPanel1, ID_WXPANEL12, wxPoint(5, 47), wxSize(123, 32));
	WxGridSizer2->Add(WxPanel12, 0, wxALIGN_CENTER | wxEXPAND | wxALL, 5);

	WxBoxSizer10 = new wxBoxSizer(wxHORIZONTAL);
	WxPanel12->SetSizer(WxBoxSizer10);
	WxPanel12->SetAutoLayout(true);

	WxBit1 = new wxTextCtrl(WxPanel12, ID_WXBIT1, _("0"), wxPoint(5, 5), wxSize(35, 22), wxTE_READONLY | wxTE_CENTRE, wxDefaultValidator, _("WxBit1"));
	WxBit1->SetToolTip(_("2"));
	WxBit1->SetBackgroundColour(wxColour(_("WHITE")));
	WxBoxSizer10->Add(WxBit1, 0, wxALIGN_CENTER | wxALL, 5);

	WxPanel42 = new wxPanel(WxPanel12, ID_WXPANEL42, wxPoint(45, 1), wxSize(68, 30));
	WxBoxSizer10->Add(WxPanel42, 1, wxALIGN_CENTER | 0, 0);

	WxBoxSizer42 = new wxBoxSizer(wxVERTICAL);
	WxPanel42->SetSizer(WxBoxSizer42);
	WxPanel42->SetAutoLayout(true);

	WxBit1Label = new wxStaticText(WxPanel42, ID_WXBIT1LABEL, _("1 << 1"), wxPoint(5, 5), wxSize(60, 19), wxALIGN_LEFT | wxST_NO_AUTORESIZE, _("WxBit1Label"));
	WxBit1Label->SetToolTip(_("2"));
	WxBoxSizer42->Add(WxBit1Label, 1, wxALIGN_CENTER_VERTICAL | wxEXPAND | wxALL, 5);

	WxPanel13 = new wxPanel(WxPanel1, ID_WXPANEL13, wxPoint(138, 47), wxSize(123, 32));
	WxGridSizer2->Add(WxPanel13, 0, wxALIGN_CENTER | wxEXPAND | wxALL, 5);

	WxBoxSizer11 = new wxBoxSizer(wxHORIZONTAL);
	WxPanel13->SetSizer(WxBoxSizer11);
	WxPanel13->SetAutoLayout(true);

	WxBit9 = new wxTextCtrl(WxPanel13, ID_WXBIT9, _("0"), wxPoint(5, 5), wxSize(35, 22), wxTE_READONLY | wxTE_CENTRE, wxDefaultValidator, _("WxBit9"));
	WxBit9->SetToolTip(_("512"));
	WxBit9->SetBackgroundColour(wxColour(_("WHITE")));
	WxBoxSizer11->Add(WxBit9, 0, wxALIGN_CENTER | wxALL, 5);

	WxPanel43 = new wxPanel(WxPanel13, ID_WXPANEL43, wxPoint(45, 1), wxSize(68, 30));
	WxBoxSizer11->Add(WxPanel43, 1, wxALIGN_CENTER | 0, 0);

	WxBoxSizer43 = new wxBoxSizer(wxVERTICAL);
	WxPanel43->SetSizer(WxBoxSizer43);
	WxPanel43->SetAutoLayout(true);

	WxBit9Label = new wxStaticText(WxPanel43, ID_WXBIT9LABEL, _("1 << 9"), wxPoint(5, 5), wxSize(60, 19), wxALIGN_LEFT | wxST_NO_AUTORESIZE, _("WxBit9Label"));
	WxBit9Label->SetToolTip(_("512"));
	WxBoxSizer43->Add(WxBit9Label, 1, wxALIGN_CENTER | wxEXPAND | wxALL, 5);

	WxPanel14 = new wxPanel(WxPanel1, ID_WXPANEL14, wxPoint(271, 47), wxSize(123, 32));
	WxGridSizer2->Add(WxPanel14, 0, wxALIGN_CENTER | wxEXPAND | wxALL, 5);

	WxBoxSizer12 = new wxBoxSizer(wxHORIZONTAL);
	WxPanel14->SetSizer(WxBoxSizer12);
	WxPanel14->SetAutoLayout(true);

	WxBit17 = new wxTextCtrl(WxPanel14, ID_WXBIT17, _("0"), wxPoint(5, 5), wxSize(35, 22), wxTE_READONLY | wxTE_CENTRE, wxDefaultValidator, _("WxBit17"));
	WxBit17->SetToolTip(_("131072"));
	WxBit17->SetBackgroundColour(wxColour(_("WHITE")));
	WxBoxSizer12->Add(WxBit17, 0, wxALIGN_CENTER | wxALL, 5);

	WxPanel44 = new wxPanel(WxPanel14, ID_WXPANEL44, wxPoint(45, 1), wxSize(68, 30));
	WxBoxSizer12->Add(WxPanel44, 1, wxALIGN_CENTER | 0, 0);

	WxBoxSizer44 = new wxBoxSizer(wxVERTICAL);
	WxPanel44->SetSizer(WxBoxSizer44);
	WxPanel44->SetAutoLayout(true);

	WxBit17Label = new wxStaticText(WxPanel44, ID_WXBIT17LABEL, _("1 << 17"), wxPoint(5, 5), wxSize(60, 19), wxALIGN_LEFT | wxST_NO_AUTORESIZE, _("WxBit17Label"));
	WxBit17Label->SetToolTip(_("131072"));
	WxBoxSizer44->Add(WxBit17Label, 1, wxALIGN_CENTER | wxEXPAND | wxALL, 5);

	WxPanel15 = new wxPanel(WxPanel1, ID_WXPANEL15, wxPoint(404, 47), wxSize(123, 32));
	WxGridSizer2->Add(WxPanel15, 0, wxALIGN_LEFT | wxEXPAND | wxALL, 5);

	WxBoxSizer13 = new wxBoxSizer(wxHORIZONTAL);
	WxPanel15->SetSizer(WxBoxSizer13);
	WxPanel15->SetAutoLayout(true);

	WxBit25 = new wxTextCtrl(WxPanel15, ID_WXBIT25, _("0"), wxPoint(5, 5), wxSize(36, 22), wxTE_READONLY | wxTE_CENTRE, wxDefaultValidator, _("WxBit25"));
	WxBit25->SetToolTip(_("33554432"));
	WxBit25->SetBackgroundColour(wxColour(_("WHITE")));
	WxBoxSizer13->Add(WxBit25, 0, wxALIGN_CENTER | wxALL, 5);

	WxPanel45 = new wxPanel(WxPanel15, ID_WXPANEL45, wxPoint(46, 1), wxSize(68, 30));
	WxBoxSizer13->Add(WxPanel45, 1, wxALIGN_CENTER | 0, 0);

	WxBoxSizer45 = new wxBoxSizer(wxVERTICAL);
	WxPanel45->SetSizer(WxBoxSizer45);
	WxPanel45->SetAutoLayout(true);

	WxBit25Label = new wxStaticText(WxPanel45, ID_WXBIT25LABEL, _("1 << 25"), wxPoint(5, 5), wxSize(60, 19), wxALIGN_LEFT | wxST_NO_AUTORESIZE, _("WxBit25Label"));
	WxBit25Label->SetToolTip(_("33554432"));
	WxBoxSizer45->Add(WxBit25Label, 1, wxALIGN_CENTER | wxEXPAND | wxALL, 5);

	WxPanel16 = new wxPanel(WxPanel1, ID_WXPANEL16, wxPoint(5, 89), wxSize(123, 32));
	WxGridSizer2->Add(WxPanel16, 0, wxALIGN_CENTER | wxEXPAND | wxALL, 5);

	WxBoxSizer14 = new wxBoxSizer(wxHORIZONTAL);
	WxPanel16->SetSizer(WxBoxSizer14);
	WxPanel16->SetAutoLayout(true);

	WxBit2 = new wxTextCtrl(WxPanel16, ID_WXBIT2, _("0"), wxPoint(5, 5), wxSize(35, 22), wxTE_READONLY | wxTE_CENTRE, wxDefaultValidator, _("WxBit2"));
	WxBit2->SetToolTip(_("4"));
	WxBit2->SetBackgroundColour(wxColour(_("WHITE")));
	WxBoxSizer14->Add(WxBit2, 0, wxALIGN_CENTER | wxALL, 5);

	WxPanel46 = new wxPanel(WxPanel16, ID_WXPANEL46, wxPoint(45, 1), wxSize(68, 30));
	WxBoxSizer14->Add(WxPanel46, 1, wxALIGN_CENTER | 0, 0);

	WxBoxSizer46 = new wxBoxSizer(wxVERTICAL);
	WxPanel46->SetSizer(WxBoxSizer46);
	WxPanel46->SetAutoLayout(true);

	WxBit2Label = new wxStaticText(WxPanel46, ID_WXBIT2LABEL, _("1 << 2"), wxPoint(5, 5), wxSize(60, 19), wxALIGN_LEFT | wxST_NO_AUTORESIZE, _("WxBit2Label"));
	WxBit2Label->SetToolTip(_("4"));
	WxBoxSizer46->Add(WxBit2Label, 1, wxALIGN_CENTER | wxEXPAND | wxALL, 5);

	WxPanel17 = new wxPanel(WxPanel1, ID_WXPANEL17, wxPoint(138, 89), wxSize(123, 32));
	WxGridSizer2->Add(WxPanel17, 0, wxALIGN_CENTER | wxEXPAND | wxALL, 5);

	WxBoxSizer15 = new wxBoxSizer(wxHORIZONTAL);
	WxPanel17->SetSizer(WxBoxSizer15);
	WxPanel17->SetAutoLayout(true);

	WxBit10 = new wxTextCtrl(WxPanel17, ID_WXBIT10, _("0"), wxPoint(5, 5), wxSize(35, 22), wxTE_READONLY | wxTE_CENTRE, wxDefaultValidator, _("WxBit10"));
	WxBit10->SetToolTip(_("1024"));
	WxBit10->SetBackgroundColour(wxColour(_("WHITE")));
	WxBoxSizer15->Add(WxBit10, 0, wxALIGN_CENTER | wxALL, 5);

	WxPanel47 = new wxPanel(WxPanel17, ID_WXPANEL47, wxPoint(45, 1), wxSize(68, 30));
	WxBoxSizer15->Add(WxPanel47, 1, wxALIGN_CENTER | 0, 0);

	WxBoxSizer47 = new wxBoxSizer(wxVERTICAL);
	WxPanel47->SetSizer(WxBoxSizer47);
	WxPanel47->SetAutoLayout(true);

	WxBit10Label = new wxStaticText(WxPanel47, ID_WXBIT10LABEL, _("1 << 10"), wxPoint(5, 5), wxSize(60, 19), wxALIGN_LEFT | wxST_NO_AUTORESIZE, _("WxBit10Label"));
	WxBit10Label->SetToolTip(_("1024"));
	WxBoxSizer47->Add(WxBit10Label, 1, wxALIGN_CENTER | wxEXPAND | wxALL, 5);

	WxPanel18 = new wxPanel(WxPanel1, ID_WXPANEL18, wxPoint(271, 89), wxSize(123, 32));
	WxGridSizer2->Add(WxPanel18, 0, wxALIGN_CENTER | wxEXPAND | wxALL, 5);

	WxBoxSizer16 = new wxBoxSizer(wxHORIZONTAL);
	WxPanel18->SetSizer(WxBoxSizer16);
	WxPanel18->SetAutoLayout(true);

	WxBit18 = new wxTextCtrl(WxPanel18, ID_WXBIT18, _("0"), wxPoint(5, 5), wxSize(35, 22), wxTE_READONLY | wxTE_CENTRE, wxDefaultValidator, _("WxBit18"));
	WxBit18->SetToolTip(_("262144"));
	WxBit18->SetBackgroundColour(wxColour(_("WHITE")));
	WxBoxSizer16->Add(WxBit18, 0, wxALIGN_CENTER | wxALL, 5);

	WxPanel48 = new wxPanel(WxPanel18, ID_WXPANEL48, wxPoint(45, 1), wxSize(68, 30));
	WxBoxSizer16->Add(WxPanel48, 1, wxALIGN_CENTER | 0, 0);

	WxBoxSizer48 = new wxBoxSizer(wxVERTICAL);
	WxPanel48->SetSizer(WxBoxSizer48);
	WxPanel48->SetAutoLayout(true);

	WxBit18Label = new wxStaticText(WxPanel48, ID_WXBIT18LABEL, _("1 << 18"), wxPoint(5, 5), wxSize(60, 19), wxALIGN_LEFT | wxST_NO_AUTORESIZE, _("WxBit18Label"));
	WxBit18Label->SetToolTip(_("262144"));
	WxBoxSizer48->Add(WxBit18Label, 1, wxALIGN_CENTER | wxEXPAND | wxALL, 5);

	WxPanel19 = new wxPanel(WxPanel1, ID_WXPANEL19, wxPoint(404, 89), wxSize(123, 32));
	WxGridSizer2->Add(WxPanel19, 0, wxALIGN_CENTER | wxEXPAND | wxALL, 5);

	WxBoxSizer17 = new wxBoxSizer(wxHORIZONTAL);
	WxPanel19->SetSizer(WxBoxSizer17);
	WxPanel19->SetAutoLayout(true);

	WxBit26 = new wxTextCtrl(WxPanel19, ID_WXBIT26, _("0"), wxPoint(5, 5), wxSize(36, 22), wxTE_READONLY | wxTE_CENTRE, wxDefaultValidator, _("WxBit26"));
	WxBit26->SetToolTip(_("67108864"));
	WxBit26->SetBackgroundColour(wxColour(_("WHITE")));
	WxBoxSizer17->Add(WxBit26, 0, wxALIGN_CENTER | wxALL, 5);

	WxPanel49 = new wxPanel(WxPanel19, ID_WXPANEL49, wxPoint(46, 1), wxSize(68, 30));
	WxBoxSizer17->Add(WxPanel49, 1, wxALIGN_CENTER | 0, 0);

	WxBoxSizer49 = new wxBoxSizer(wxVERTICAL);
	WxPanel49->SetSizer(WxBoxSizer49);
	WxPanel49->SetAutoLayout(true);

	WxBit26Label = new wxStaticText(WxPanel49, ID_WXBIT26LABEL, _("1 << 26"), wxPoint(5, 5), wxSize(60, 19), wxALIGN_LEFT | wxST_NO_AUTORESIZE, _("WxBit26Label"));
	WxBit26Label->SetToolTip(_("67108864"));
	WxBoxSizer49->Add(WxBit26Label, 1, wxALIGN_CENTER | wxEXPAND | wxALL, 5);

	WxPanel20 = new wxPanel(WxPanel1, ID_WXPANEL20, wxPoint(5, 131), wxSize(123, 32));
	WxGridSizer2->Add(WxPanel20, 0, wxALIGN_CENTER | wxEXPAND | wxALL, 5);

	WxBoxSizer18 = new wxBoxSizer(wxHORIZONTAL);
	WxPanel20->SetSizer(WxBoxSizer18);
	WxPanel20->SetAutoLayout(true);

	WxBit3 = new wxTextCtrl(WxPanel20, ID_WXBIT3, _("0"), wxPoint(5, 5), wxSize(35, 22), wxTE_READONLY | wxTE_CENTRE, wxDefaultValidator, _("WxBit3"));
	WxBit3->SetToolTip(_("8"));
	WxBit3->SetBackgroundColour(wxColour(_("WHITE")));
	WxBoxSizer18->Add(WxBit3, 0, wxALIGN_CENTER | wxALL, 5);

	WxPanel50 = new wxPanel(WxPanel20, ID_WXPANEL50, wxPoint(45, 1), wxSize(68, 30));
	WxBoxSizer18->Add(WxPanel50, 1, wxALIGN_CENTER | 0, 0);

	WxBoxSizer50 = new wxBoxSizer(wxVERTICAL);
	WxPanel50->SetSizer(WxBoxSizer50);
	WxPanel50->SetAutoLayout(true);

	WxBit3Label = new wxStaticText(WxPanel50, ID_WXBIT3LABEL, _("1 << 3"), wxPoint(5, 5), wxSize(60, 19), wxALIGN_LEFT | wxST_NO_AUTORESIZE, _("WxBit3Label"));
	WxBit3Label->SetToolTip(_("8"));
	WxBoxSizer50->Add(WxBit3Label, 1, wxALIGN_CENTER | wxEXPAND | wxALL, 5);

	WxPanel21 = new wxPanel(WxPanel1, ID_WXPANEL21, wxPoint(138, 131), wxSize(123, 32));
	WxGridSizer2->Add(WxPanel21, 0, wxALIGN_CENTER | wxEXPAND | wxALL, 5);

	WxBoxSizer19 = new wxBoxSizer(wxHORIZONTAL);
	WxPanel21->SetSizer(WxBoxSizer19);
	WxPanel21->SetAutoLayout(true);

	WxBit11 = new wxTextCtrl(WxPanel21, ID_WXBIT11, _("0"), wxPoint(5, 5), wxSize(35, 22), wxTE_READONLY | wxTE_CENTRE, wxDefaultValidator, _("WxBit11"));
	WxBit11->SetToolTip(_("2048"));
	WxBit11->SetBackgroundColour(wxColour(_("WHITE")));
	WxBoxSizer19->Add(WxBit11, 0, wxALIGN_CENTER | wxALL, 5);

	WxPanel51 = new wxPanel(WxPanel21, ID_WXPANEL51, wxPoint(45, 1), wxSize(68, 30));
	WxBoxSizer19->Add(WxPanel51, 1, wxALIGN_CENTER | 0, 0);

	WxBoxSizer51 = new wxBoxSizer(wxVERTICAL);
	WxPanel51->SetSizer(WxBoxSizer51);
	WxPanel51->SetAutoLayout(true);

	WxBit11Label = new wxStaticText(WxPanel51, ID_WXBIT11LABEL, _("1 << 11"), wxPoint(5, 5), wxSize(60, 19), wxALIGN_LEFT | wxST_NO_AUTORESIZE, _("WxBit11Label"));
	WxBit11Label->SetToolTip(_("2048"));
	WxBoxSizer51->Add(WxBit11Label, 1, wxALIGN_CENTER | wxEXPAND | wxALL, 5);

	WxPanel22 = new wxPanel(WxPanel1, ID_WXPANEL22, wxPoint(271, 131), wxSize(123, 32));
	WxGridSizer2->Add(WxPanel22, 0, wxALIGN_CENTER | wxEXPAND | wxALL, 5);

	WxBoxSizer20 = new wxBoxSizer(wxHORIZONTAL);
	WxPanel22->SetSizer(WxBoxSizer20);
	WxPanel22->SetAutoLayout(true);

	WxBit19 = new wxTextCtrl(WxPanel22, ID_WXBIT19, _("0"), wxPoint(5, 5), wxSize(35, 22), wxTE_READONLY | wxTE_CENTRE, wxDefaultValidator, _("WxBit19"));
	WxBit19->SetToolTip(_("524288"));
	WxBit19->SetBackgroundColour(wxColour(_("WHITE")));
	WxBoxSizer20->Add(WxBit19, 0, wxALIGN_CENTER | wxALL, 5);

	WxPanel52 = new wxPanel(WxPanel22, ID_WXPANEL52, wxPoint(45, 1), wxSize(68, 30));
	WxBoxSizer20->Add(WxPanel52, 1, wxALIGN_CENTER | 0, 0);

	WxBoxSizer52 = new wxBoxSizer(wxVERTICAL);
	WxPanel52->SetSizer(WxBoxSizer52);
	WxPanel52->SetAutoLayout(true);

	WxBit19Label = new wxStaticText(WxPanel52, ID_WXBIT19LABEL, _("1 << 19"), wxPoint(5, 5), wxSize(60, 19), wxALIGN_LEFT | wxST_NO_AUTORESIZE, _("WxBit19Label"));
	WxBit19Label->SetToolTip(_("524288"));
	WxBoxSizer52->Add(WxBit19Label, 1, wxALIGN_CENTER | wxEXPAND | wxALL, 5);

	WxPanel23 = new wxPanel(WxPanel1, ID_WXPANEL23, wxPoint(404, 131), wxSize(123, 32));
	WxGridSizer2->Add(WxPanel23, 0, wxALIGN_CENTER | wxEXPAND | wxALL, 5);

	WxBoxSizer21 = new wxBoxSizer(wxHORIZONTAL);
	WxPanel23->SetSizer(WxBoxSizer21);
	WxPanel23->SetAutoLayout(true);

	WxBit27 = new wxTextCtrl(WxPanel23, ID_WXBIT27, _("0"), wxPoint(5, 5), wxSize(36, 22), wxTE_READONLY | wxTE_CENTRE, wxDefaultValidator, _("WxBit27"));
	WxBit27->SetToolTip(_("134217728"));
	WxBit27->SetBackgroundColour(wxColour(_("WHITE")));
	WxBoxSizer21->Add(WxBit27, 0, wxALIGN_CENTER | wxALL, 5);

	WxPanel53 = new wxPanel(WxPanel23, ID_WXPANEL53, wxPoint(46, 1), wxSize(68, 30));
	WxBoxSizer21->Add(WxPanel53, 1, wxALIGN_CENTER | 0, 0);

	WxBoxSizer53 = new wxBoxSizer(wxVERTICAL);
	WxPanel53->SetSizer(WxBoxSizer53);
	WxPanel53->SetAutoLayout(true);

	WxBit27Label = new wxStaticText(WxPanel53, ID_WXBIT27LABEL, _("1 << 27"), wxPoint(5, 5), wxSize(60, 19), wxALIGN_LEFT | wxST_NO_AUTORESIZE, _("WxBit27Label"));
	WxBit27Label->SetToolTip(_("134217728"));
	WxBoxSizer53->Add(WxBit27Label, 1, wxALIGN_CENTER | wxEXPAND | wxALL, 5);

	WxPanel24 = new wxPanel(WxPanel1, ID_WXPANEL24, wxPoint(5, 173), wxSize(123, 32));
	WxGridSizer2->Add(WxPanel24, 0, wxALIGN_CENTER | wxEXPAND | wxALL, 5);

	WxBoxSizer22 = new wxBoxSizer(wxHORIZONTAL);
	WxPanel24->SetSizer(WxBoxSizer22);
	WxPanel24->SetAutoLayout(true);

	WxBit4 = new wxTextCtrl(WxPanel24, ID_WXBIT4, _("0"), wxPoint(5, 5), wxSize(35, 22), wxTE_READONLY | wxTE_CENTRE, wxDefaultValidator, _("WxBit4"));
	WxBit4->SetToolTip(_("16"));
	WxBit4->SetBackgroundColour(wxColour(_("WHITE")));
	WxBoxSizer22->Add(WxBit4, 0, wxALIGN_CENTER | wxALL, 5);

	WxPanel54 = new wxPanel(WxPanel24, ID_WXPANEL54, wxPoint(45, 1), wxSize(68, 30));
	WxBoxSizer22->Add(WxPanel54, 1, wxALIGN_CENTER | 0, 0);

	WxBoxSizer54 = new wxBoxSizer(wxVERTICAL);
	WxPanel54->SetSizer(WxBoxSizer54);
	WxPanel54->SetAutoLayout(true);

	WxBit4Label = new wxStaticText(WxPanel54, ID_WXBIT4LABEL, _("1 << 4"), wxPoint(5, 5), wxSize(60, 20), wxALIGN_LEFT | wxST_NO_AUTORESIZE, _("WxBit4Label"));
	WxBit4Label->SetToolTip(_("16"));
	WxBoxSizer54->Add(WxBit4Label, 1, wxALIGN_CENTER | wxEXPAND | wxALL, 5);

	WxPanel25 = new wxPanel(WxPanel1, ID_WXPANEL25, wxPoint(138, 173), wxSize(123, 32));
	WxGridSizer2->Add(WxPanel25, 0, wxALIGN_CENTER | wxEXPAND | wxALL, 5);

	WxBoxSizer23 = new wxBoxSizer(wxHORIZONTAL);
	WxPanel25->SetSizer(WxBoxSizer23);
	WxPanel25->SetAutoLayout(true);

	WxBit12 = new wxTextCtrl(WxPanel25, ID_WXBIT12, _("0"), wxPoint(5, 5), wxSize(35, 22), wxTE_READONLY | wxTE_CENTRE, wxDefaultValidator, _("WxBit12"));
	WxBit12->SetToolTip(_("4096"));
	WxBit12->SetBackgroundColour(wxColour(_("WHITE")));
	WxBoxSizer23->Add(WxBit12, 0, wxALIGN_CENTER | wxALL, 5);

	WxPanel55 = new wxPanel(WxPanel25, ID_WXPANEL55, wxPoint(45, 1), wxSize(68, 30));
	WxBoxSizer23->Add(WxPanel55, 1, wxALIGN_CENTER | 0, 0);

	WxBoxSizer55 = new wxBoxSizer(wxVERTICAL);
	WxPanel55->SetSizer(WxBoxSizer55);
	WxPanel55->SetAutoLayout(true);

	WxBit12Label = new wxStaticText(WxPanel55, ID_WXBIT12LABEL, _("1 << 12"), wxPoint(5, 5), wxSize(60, 20), wxALIGN_LEFT | wxST_NO_AUTORESIZE, _("WxBit12Label"));
	WxBit12Label->SetToolTip(_("4096"));
	WxBoxSizer55->Add(WxBit12Label, 1, wxALIGN_CENTER | wxEXPAND | wxALL, 5);

	WxPanel26 = new wxPanel(WxPanel1, ID_WXPANEL26, wxPoint(271, 173), wxSize(123, 32));
	WxGridSizer2->Add(WxPanel26, 0, wxALIGN_CENTER | wxEXPAND | wxALL, 5);

	WxBoxSizer24 = new wxBoxSizer(wxHORIZONTAL);
	WxPanel26->SetSizer(WxBoxSizer24);
	WxPanel26->SetAutoLayout(true);

	WxBit20 = new wxTextCtrl(WxPanel26, ID_WXBIT20, _("0"), wxPoint(5, 5), wxSize(35, 22), wxTE_READONLY | wxTE_CENTRE, wxDefaultValidator, _("WxBit20"));
	WxBit20->SetToolTip(_("1048576"));
	WxBit20->SetBackgroundColour(wxColour(_("WHITE")));
	WxBoxSizer24->Add(WxBit20, 0, wxALIGN_CENTER | wxALL, 5);

	WxPanel56 = new wxPanel(WxPanel26, ID_WXPANEL56, wxPoint(45, 1), wxSize(68, 30));
	WxBoxSizer24->Add(WxPanel56, 1, wxALIGN_CENTER | 0, 0);

	WxBoxSizer56 = new wxBoxSizer(wxVERTICAL);
	WxPanel56->SetSizer(WxBoxSizer56);
	WxPanel56->SetAutoLayout(true);

	WxBit20Label = new wxStaticText(WxPanel56, ID_WXBIT20LABEL, _("1 << 20"), wxPoint(5, 5), wxSize(60, 20), wxALIGN_LEFT | wxST_NO_AUTORESIZE, _("WxBit20Label"));
	WxBit20Label->SetToolTip(_("1048576"));
	WxBoxSizer56->Add(WxBit20Label, 1, wxALIGN_CENTER | wxEXPAND | wxALL, 5);

	WxPanel27 = new wxPanel(WxPanel1, ID_WXPANEL27, wxPoint(404, 173), wxSize(123, 32));
	WxGridSizer2->Add(WxPanel27, 0, wxALIGN_CENTER | wxEXPAND | wxALL, 5);

	WxBoxSizer25 = new wxBoxSizer(wxHORIZONTAL);
	WxPanel27->SetSizer(WxBoxSizer25);
	WxPanel27->SetAutoLayout(true);

	WxBit28 = new wxTextCtrl(WxPanel27, ID_WXBIT28, _("0"), wxPoint(5, 5), wxSize(36, 22), wxTE_READONLY | wxTE_CENTRE, wxDefaultValidator, _("WxBit28"));
	WxBit28->SetToolTip(_("268435456"));
	WxBit28->SetBackgroundColour(wxColour(_("WHITE")));
	WxBoxSizer25->Add(WxBit28, 0, wxALIGN_CENTER | wxALL, 5);

	WxPanel57 = new wxPanel(WxPanel27, ID_WXPANEL57, wxPoint(46, 1), wxSize(68, 30));
	WxBoxSizer25->Add(WxPanel57, 1, wxALIGN_CENTER | 0, 0);

	WxBoxSizer57 = new wxBoxSizer(wxVERTICAL);
	WxPanel57->SetSizer(WxBoxSizer57);
	WxPanel57->SetAutoLayout(true);

	WxBit28Label = new wxStaticText(WxPanel57, ID_WXBIT28LABEL, _("1 << 28"), wxPoint(5, 5), wxSize(60, 20), wxALIGN_LEFT | wxST_NO_AUTORESIZE, _("WxBit28Label"));
	WxBit28Label->SetToolTip(_("268435456"));
	WxBoxSizer57->Add(WxBit28Label, 1, wxALIGN_CENTER | wxEXPAND | wxALL, 5);

	WxPanel28 = new wxPanel(WxPanel1, ID_WXPANEL28, wxPoint(5, 215), wxSize(123, 32));
	WxGridSizer2->Add(WxPanel28, 0, wxALIGN_CENTER | wxEXPAND | wxALL, 5);

	WxBoxSizer26 = new wxBoxSizer(wxHORIZONTAL);
	WxPanel28->SetSizer(WxBoxSizer26);
	WxPanel28->SetAutoLayout(true);

	WxBit5 = new wxTextCtrl(WxPanel28, ID_WXBIT5, _("0"), wxPoint(5, 5), wxSize(35, 22), wxTE_READONLY | wxTE_CENTRE, wxDefaultValidator, _("WxBit5"));
	WxBit5->SetToolTip(_("32"));
	WxBit5->SetBackgroundColour(wxColour(_("WHITE")));
	WxBoxSizer26->Add(WxBit5, 0, wxALIGN_CENTER | wxALL, 5);

	WxPanel58 = new wxPanel(WxPanel28, ID_WXPANEL58, wxPoint(45, 1), wxSize(68, 30));
	WxBoxSizer26->Add(WxPanel58, 1, wxALIGN_CENTER | 0, 0);

	WxBoxSizer58 = new wxBoxSizer(wxVERTICAL);
	WxPanel58->SetSizer(WxBoxSizer58);
	WxPanel58->SetAutoLayout(true);

	WxBit5Label = new wxStaticText(WxPanel58, ID_WXBIT5LABEL, _("1 << 5"), wxPoint(5, 5), wxSize(60, 19), wxALIGN_LEFT | wxST_NO_AUTORESIZE, _("WxBit5Label"));
	WxBit5Label->SetToolTip(_("32"));
	WxBoxSizer58->Add(WxBit5Label, 1, wxALIGN_CENTER | wxEXPAND | wxALL, 5);

	WxPanel29 = new wxPanel(WxPanel1, ID_WXPANEL29, wxPoint(138, 215), wxSize(123, 32));
	WxGridSizer2->Add(WxPanel29, 0, wxALIGN_CENTER | wxEXPAND | wxALL, 5);

	WxBoxSizer27 = new wxBoxSizer(wxHORIZONTAL);
	WxPanel29->SetSizer(WxBoxSizer27);
	WxPanel29->SetAutoLayout(true);

	WxBit13 = new wxTextCtrl(WxPanel29, ID_WXBIT13, _("0"), wxPoint(5, 5), wxSize(35, 22), wxTE_READONLY | wxTE_CENTRE, wxDefaultValidator, _("WxBit13"));
	WxBit13->SetToolTip(_("8192"));
	WxBit13->SetBackgroundColour(wxColour(_("WHITE")));
	WxBoxSizer27->Add(WxBit13, 0, wxALIGN_CENTER | wxALL, 5);

	WxPanel59 = new wxPanel(WxPanel29, ID_WXPANEL59, wxPoint(45, 1), wxSize(68, 30));
	WxBoxSizer27->Add(WxPanel59, 1, wxALIGN_CENTER | 0, 0);

	WxBoxSizer59 = new wxBoxSizer(wxVERTICAL);
	WxPanel59->SetSizer(WxBoxSizer59);
	WxPanel59->SetAutoLayout(true);

	WxBit13Label = new wxStaticText(WxPanel59, ID_WXBIT13LABEL, _("1 << 13"), wxPoint(5, 5), wxSize(60, 19), wxALIGN_LEFT | wxST_NO_AUTORESIZE, _("WxBit13Label"));
	WxBit13Label->SetToolTip(_("8192"));
	WxBoxSizer59->Add(WxBit13Label, 1, wxALIGN_CENTER | wxEXPAND | wxALL, 5);

	WxPanel30 = new wxPanel(WxPanel1, ID_WXPANEL30, wxPoint(271, 215), wxSize(123, 32));
	WxGridSizer2->Add(WxPanel30, 0, wxALIGN_CENTER | wxEXPAND | wxALL, 5);

	WxBoxSizer28 = new wxBoxSizer(wxHORIZONTAL);
	WxPanel30->SetSizer(WxBoxSizer28);
	WxPanel30->SetAutoLayout(true);

	WxBit21 = new wxTextCtrl(WxPanel30, ID_WXBIT21, _("0"), wxPoint(5, 5), wxSize(35, 22), wxTE_READONLY | wxTE_CENTRE, wxDefaultValidator, _("WxBit21"));
	WxBit21->SetToolTip(_("2097152"));
	WxBit21->SetBackgroundColour(wxColour(_("WHITE")));
	WxBoxSizer28->Add(WxBit21, 0, wxALIGN_CENTER | wxALL, 5);

	WxPanel60 = new wxPanel(WxPanel30, ID_WXPANEL60, wxPoint(45, 1), wxSize(68, 30));
	WxBoxSizer28->Add(WxPanel60, 1, wxALIGN_CENTER | 0, 0);

	WxBoxSizer60 = new wxBoxSizer(wxVERTICAL);
	WxPanel60->SetSizer(WxBoxSizer60);
	WxPanel60->SetAutoLayout(true);

	WxBit21Label = new wxStaticText(WxPanel60, ID_WXBIT21LABEL, _("1 << 21"), wxPoint(5, 5), wxSize(60, 19), wxALIGN_LEFT | wxST_NO_AUTORESIZE, _("WxBit21Label"));
	WxBit21Label->SetToolTip(_("2097152"));
	WxBoxSizer60->Add(WxBit21Label, 1, wxALIGN_CENTER | wxEXPAND | wxALL, 5);

	WxPanel31 = new wxPanel(WxPanel1, ID_WXPANEL31, wxPoint(404, 215), wxSize(123, 32));
	WxGridSizer2->Add(WxPanel31, 0, wxALIGN_CENTER | wxEXPAND | wxALL, 5);

	WxBoxSizer29 = new wxBoxSizer(wxHORIZONTAL);
	WxPanel31->SetSizer(WxBoxSizer29);
	WxPanel31->SetAutoLayout(true);

	WxBit29 = new wxTextCtrl(WxPanel31, ID_WXBIT29, _("0"), wxPoint(5, 5), wxSize(36, 22), wxTE_READONLY | wxTE_CENTRE, wxDefaultValidator, _("WxBit29"));
	WxBit29->SetToolTip(_("536870912"));
	WxBit29->SetBackgroundColour(wxColour(_("WHITE")));
	WxBoxSizer29->Add(WxBit29, 0, wxALIGN_CENTER | wxALL, 5);

	WxPanel61 = new wxPanel(WxPanel31, ID_WXPANEL61, wxPoint(46, 1), wxSize(68, 30));
	WxBoxSizer29->Add(WxPanel61, 1, wxALIGN_CENTER | 0, 0);

	WxBoxSizer61 = new wxBoxSizer(wxVERTICAL);
	WxPanel61->SetSizer(WxBoxSizer61);
	WxPanel61->SetAutoLayout(true);

	WxBit29Label = new wxStaticText(WxPanel61, ID_WXBIT29LABEL, _("1 << 29"), wxPoint(5, 5), wxSize(60, 19), wxALIGN_LEFT | wxST_NO_AUTORESIZE, _("WxBit29Label"));
	WxBit29Label->SetToolTip(_("536870912"));
	WxBoxSizer61->Add(WxBit29Label, 1, wxALIGN_CENTER | wxEXPAND | wxALL, 5);

	WxPanel32 = new wxPanel(WxPanel1, ID_WXPANEL32, wxPoint(5, 257), wxSize(123, 32));
	WxGridSizer2->Add(WxPanel32, 0, wxALIGN_CENTER | wxEXPAND | wxALL, 5);

	WxBoxSizer30 = new wxBoxSizer(wxHORIZONTAL);
	WxPanel32->SetSizer(WxBoxSizer30);
	WxPanel32->SetAutoLayout(true);

	WxBit6 = new wxTextCtrl(WxPanel32, ID_WXBIT6, _("0"), wxPoint(5, 5), wxSize(35, 22), wxTE_READONLY | wxTE_CENTRE, wxDefaultValidator, _("WxBit6"));
	WxBit6->SetToolTip(_("64"));
	WxBit6->SetBackgroundColour(wxColour(_("WHITE")));
	WxBoxSizer30->Add(WxBit6, 0, wxALIGN_CENTER | wxALL, 5);

	WxPanel62 = new wxPanel(WxPanel32, ID_WXPANEL62, wxPoint(45, 1), wxSize(68, 30));
	WxBoxSizer30->Add(WxPanel62, 1, wxALIGN_CENTER | 0, 0);

	WxBoxSizer62 = new wxBoxSizer(wxVERTICAL);
	WxPanel62->SetSizer(WxBoxSizer62);
	WxPanel62->SetAutoLayout(true);

	WxBit6Label = new wxStaticText(WxPanel62, ID_WXBIT6LABEL, _("1 << 6"), wxPoint(5, 5), wxSize(60, 19), wxALIGN_LEFT | wxST_NO_AUTORESIZE, _("WxBit6Label"));
	WxBit6Label->SetToolTip(_("64"));
	WxBoxSizer62->Add(WxBit6Label, 1, wxALIGN_CENTER | wxEXPAND | wxALL, 5);

	WxPanel33 = new wxPanel(WxPanel1, ID_WXPANEL33, wxPoint(138, 257), wxSize(123, 32));
	WxGridSizer2->Add(WxPanel33, 0, wxALIGN_CENTER | wxEXPAND | wxALL, 5);

	WxBoxSizer31 = new wxBoxSizer(wxHORIZONTAL);
	WxPanel33->SetSizer(WxBoxSizer31);
	WxPanel33->SetAutoLayout(true);

	WxBit14 = new wxTextCtrl(WxPanel33, ID_WXBIT14, _("0"), wxPoint(5, 5), wxSize(35, 22), wxTE_READONLY | wxTE_CENTRE, wxDefaultValidator, _("WxBit14"));
	WxBit14->SetToolTip(_("16384"));
	WxBit14->SetBackgroundColour(wxColour(_("WHITE")));
	WxBoxSizer31->Add(WxBit14, 0, wxALIGN_CENTER | wxALL, 5);

	WxPanel63 = new wxPanel(WxPanel33, ID_WXPANEL63, wxPoint(45, 1), wxSize(68, 30));
	WxBoxSizer31->Add(WxPanel63, 1, wxALIGN_CENTER | 0, 0);

	WxBoxSizer63 = new wxBoxSizer(wxVERTICAL);
	WxPanel63->SetSizer(WxBoxSizer63);
	WxPanel63->SetAutoLayout(true);

	WxBit14Label = new wxStaticText(WxPanel63, ID_WXBIT14LABEL, _("1 << 14"), wxPoint(5, 5), wxSize(60, 19), wxALIGN_LEFT | wxST_NO_AUTORESIZE, _("WxBit14Label"));
	WxBit14Label->SetToolTip(_("16384"));
	WxBoxSizer63->Add(WxBit14Label, 1, wxALIGN_CENTER | wxEXPAND | wxALL, 5);

	WxPanel34 = new wxPanel(WxPanel1, ID_WXPANEL34, wxPoint(271, 257), wxSize(123, 32));
	WxGridSizer2->Add(WxPanel34, 0, wxALIGN_CENTER | wxEXPAND | wxALL, 5);

	WxBoxSizer32 = new wxBoxSizer(wxHORIZONTAL);
	WxPanel34->SetSizer(WxBoxSizer32);
	WxPanel34->SetAutoLayout(true);

	WxBit22 = new wxTextCtrl(WxPanel34, ID_WXBIT22, _("0"), wxPoint(5, 5), wxSize(35, 22), wxTE_READONLY | wxTE_CENTRE, wxDefaultValidator, _("WxBit22"));
	WxBit22->SetToolTip(_("4194304"));
	WxBit22->SetBackgroundColour(wxColour(_("WHITE")));
	WxBoxSizer32->Add(WxBit22, 0, wxALIGN_CENTER | wxALL, 5);

	WxPanel64 = new wxPanel(WxPanel34, ID_WXPANEL64, wxPoint(45, 1), wxSize(68, 30));
	WxBoxSizer32->Add(WxPanel64, 1, wxALIGN_CENTER | 0, 0);

	WxBoxSizer64 = new wxBoxSizer(wxVERTICAL);
	WxPanel64->SetSizer(WxBoxSizer64);
	WxPanel64->SetAutoLayout(true);

	WxBit22Label = new wxStaticText(WxPanel64, ID_WXBIT22LABEL, _("1 << 22"), wxPoint(5, 5), wxSize(60, 19), wxALIGN_LEFT | wxST_NO_AUTORESIZE, _("WxBit22Label"));
	WxBit22Label->SetToolTip(_("4194304"));
	WxBoxSizer64->Add(WxBit22Label, 1, wxALIGN_CENTER | wxEXPAND | wxALL, 5);

	WxPanel35 = new wxPanel(WxPanel1, ID_WXPANEL35, wxPoint(404, 257), wxSize(123, 32));
	WxGridSizer2->Add(WxPanel35, 0, wxALIGN_CENTER | wxEXPAND | wxALL, 5);

	WxBoxSizer33 = new wxBoxSizer(wxHORIZONTAL);
	WxPanel35->SetSizer(WxBoxSizer33);
	WxPanel35->SetAutoLayout(true);

	WxBit30 = new wxTextCtrl(WxPanel35, ID_WXBIT30, _("0"), wxPoint(5, 5), wxSize(36, 22), wxTE_READONLY | wxTE_CENTRE, wxDefaultValidator, _("WxBit30"));
	WxBit30->SetToolTip(_("1073741824"));
	WxBit30->SetBackgroundColour(wxColour(_("WHITE")));
	WxBoxSizer33->Add(WxBit30, 0, wxALIGN_CENTER | wxALL, 5);

	WxPanel65 = new wxPanel(WxPanel35, ID_WXPANEL65, wxPoint(46, 1), wxSize(68, 30));
	WxBoxSizer33->Add(WxPanel65, 1, wxALIGN_CENTER | 0, 0);

	WxBoxSizer65 = new wxBoxSizer(wxVERTICAL);
	WxPanel65->SetSizer(WxBoxSizer65);
	WxPanel65->SetAutoLayout(true);

	WxBit30Label = new wxStaticText(WxPanel65, ID_WXBIT30LABEL, _("1 << 30"), wxPoint(5, 5), wxSize(60, 19), wxALIGN_LEFT | wxST_NO_AUTORESIZE, _("WxBit30Label"));
	WxBit30Label->SetToolTip(_("1073741824"));
	WxBoxSizer65->Add(WxBit30Label, 1, wxALIGN_CENTER | wxEXPAND | wxALL, 5);

	WxPanel36 = new wxPanel(WxPanel1, ID_WXPANEL36, wxPoint(5, 299), wxSize(123, 32));
	WxGridSizer2->Add(WxPanel36, 0, wxALIGN_CENTER | wxEXPAND | wxALL, 5);

	WxBoxSizer34 = new wxBoxSizer(wxHORIZONTAL);
	WxPanel36->SetSizer(WxBoxSizer34);
	WxPanel36->SetAutoLayout(true);

	WxBit7 = new wxTextCtrl(WxPanel36, ID_WXBIT7, _("0"), wxPoint(5, 5), wxSize(35, 21), wxTE_READONLY | wxTE_NOHIDESEL | wxTE_CENTRE, wxDefaultValidator, _("WxBit7"));
	WxBit7->SetToolTip(_("128"));
	WxBit7->SetBackgroundColour(wxColour(_("WHITE")));
	WxBoxSizer34->Add(WxBit7, 0, wxALIGN_CENTER | wxALL, 5);

	WxPanel66 = new wxPanel(WxPanel36, ID_WXPANEL66, wxPoint(45, 0), wxSize(68, 30));
	WxBoxSizer34->Add(WxPanel66, 1, wxALIGN_CENTER | 0, 0);

	WxBoxSizer66 = new wxBoxSizer(wxVERTICAL);
	WxPanel66->SetSizer(WxBoxSizer66);
	WxPanel66->SetAutoLayout(true);

	WxBit7Label = new wxStaticText(WxPanel66, ID_WXBIT7LABEL, _("1 << 7"), wxPoint(5, 5), wxSize(60, 18), wxALIGN_LEFT | wxST_NO_AUTORESIZE, _("WxBit7Label"));
	WxBit7Label->SetToolTip(_("128"));
	WxBoxSizer66->Add(WxBit7Label, 1, wxALIGN_CENTER | wxEXPAND | wxALL, 5);

	WxPanel37 = new wxPanel(WxPanel1, ID_WXPANEL37, wxPoint(138, 299), wxSize(123, 32));
	WxGridSizer2->Add(WxPanel37, 0, wxALIGN_CENTER | wxEXPAND | wxALL, 5);

	WxBoxSizer35 = new wxBoxSizer(wxHORIZONTAL);
	WxPanel37->SetSizer(WxBoxSizer35);
	WxPanel37->SetAutoLayout(true);

	WxBit15 = new wxTextCtrl(WxPanel37, ID_WXBIT15, _("0"), wxPoint(5, 5), wxSize(35, 21), wxTE_READONLY | wxTE_CENTRE, wxDefaultValidator, _("WxBit15"));
	WxBit15->SetToolTip(_("32768"));
	WxBit15->SetBackgroundColour(wxColour(_("WHITE")));
	WxBoxSizer35->Add(WxBit15, 0, wxALIGN_CENTER | wxALL, 5);

	WxPanel67 = new wxPanel(WxPanel37, ID_WXPANEL67, wxPoint(45, 0), wxSize(68, 30));
	WxBoxSizer35->Add(WxPanel67, 1, wxALIGN_CENTER | 0, 0);

	WxBoxSizer67 = new wxBoxSizer(wxVERTICAL);
	WxPanel67->SetSizer(WxBoxSizer67);
	WxPanel67->SetAutoLayout(true);

	WxBit15Label = new wxStaticText(WxPanel67, ID_WXBIT15LABEL, _("1 << 15"), wxPoint(5, 5), wxSize(60, 18), wxALIGN_LEFT | wxST_NO_AUTORESIZE, _("WxBit15Label"));
	WxBit15Label->SetToolTip(_("32768"));
	WxBoxSizer67->Add(WxBit15Label, 1, wxALIGN_CENTER | wxEXPAND | wxALL, 5);

	WxPanel38 = new wxPanel(WxPanel1, ID_WXPANEL38, wxPoint(271, 299), wxSize(123, 32));
	WxGridSizer2->Add(WxPanel38, 0, wxALIGN_CENTER | wxEXPAND | wxALL, 5);

	WxBoxSizer36 = new wxBoxSizer(wxHORIZONTAL);
	WxPanel38->SetSizer(WxBoxSizer36);
	WxPanel38->SetAutoLayout(true);

	WxBit23 = new wxTextCtrl(WxPanel38, ID_WXBIT23, _("0"), wxPoint(5, 5), wxSize(35, 21), wxTE_READONLY | wxTE_CENTRE, wxDefaultValidator, _("WxBit23"));
	WxBit23->SetToolTip(_("8388608"));
	WxBit23->SetBackgroundColour(wxColour(_("WHITE")));
	WxBoxSizer36->Add(WxBit23, 0, wxALIGN_CENTER | wxALL, 5);

	WxPanel68 = new wxPanel(WxPanel38, ID_WXPANEL68, wxPoint(45, 0), wxSize(68, 30));
	WxBoxSizer36->Add(WxPanel68, 1, wxALIGN_CENTER | 0, 0);

	WxBoxSizer68 = new wxBoxSizer(wxVERTICAL);
	WxPanel68->SetSizer(WxBoxSizer68);
	WxPanel68->SetAutoLayout(true);

	WxBit23Label = new wxStaticText(WxPanel68, ID_WXBIT23LABEL, _("1 << 23"), wxPoint(5, 5), wxSize(60, 18), wxALIGN_LEFT | wxST_NO_AUTORESIZE, _("WxBit23Label"));
	WxBit23Label->SetToolTip(_("8388608"));
	WxBoxSizer68->Add(WxBit23Label, 1, wxALIGN_CENTER | wxEXPAND | wxALL, 5);

	WxPanel39 = new wxPanel(WxPanel1, ID_WXPANEL39, wxPoint(404, 299), wxSize(123, 32));
	WxGridSizer2->Add(WxPanel39, 0, wxALIGN_CENTER | wxEXPAND | wxALL, 5);

	WxBoxSizer37 = new wxBoxSizer(wxHORIZONTAL);
	WxPanel39->SetSizer(WxBoxSizer37);
	WxPanel39->SetAutoLayout(true);

	WxBit31 = new wxTextCtrl(WxPanel39, ID_WXBIT31, _("0"), wxPoint(5, 5), wxSize(36, 21), wxTE_READONLY | wxTE_CENTRE, wxDefaultValidator, _("WxBit31"));
	WxBit31->SetToolTip(_("2147483648"));
	WxBit31->SetBackgroundColour(wxColour(_("WHITE")));
	WxBoxSizer37->Add(WxBit31, 0, wxALIGN_CENTER | wxALL, 5);

	WxPanel69 = new wxPanel(WxPanel39, ID_WXPANEL69, wxPoint(46, 0), wxSize(68, 30));
	WxBoxSizer37->Add(WxPanel69, 1, wxALIGN_CENTER | 0, 0);

	WxBoxSizer69 = new wxBoxSizer(wxVERTICAL);
	WxPanel69->SetSizer(WxBoxSizer69);
	WxPanel69->SetAutoLayout(true);

	WxBit31Label = new wxStaticText(WxPanel69, ID_WXBIT31LABEL, _("1 << 31"), wxPoint(5, 5), wxSize(60, 18), wxALIGN_LEFT | wxST_NO_AUTORESIZE, _("WxBit31Label"));
	WxBit31Label->SetToolTip(_("2147483648"));
	WxBoxSizer69->Add(WxBit31Label, 1, wxALIGN_CENTER | wxEXPAND | wxALL, 5);

	WxPanel2 = new wxPanel(this, ID_WXPANEL2, wxPoint(5, 357), wxSize(570, 50));
	WxBoxSizer1->Add(WxPanel2, 0, wxALIGN_CENTER | wxEXPAND | wxALL, 5);

	WxBoxSizer2 = new wxBoxSizer(wxHORIZONTAL);
	WxPanel2->SetSizer(WxBoxSizer2);
	WxPanel2->SetAutoLayout(true);

	WxPanel4 = new wxPanel(WxPanel2, ID_WXPANEL4, wxPoint(5, 5), wxSize(100, 36));
	WxBoxSizer2->Add(WxPanel4, 0, wxALIGN_CENTER | wxEXPAND | wxALL, 5);

	WxBoxSizer3 = new wxBoxSizer(wxHORIZONTAL);
	WxPanel4->SetSizer(WxBoxSizer3);
	WxPanel4->SetAutoLayout(true);

	WxStaticText1 = new wxStaticText(WxPanel4, ID_WXSTATICTEXT1, _("Decimal Value:"), wxPoint(5, 5), wxDefaultSize, 0, _("WxStaticText1"));
	WxBoxSizer3->Add(WxStaticText1, 0, wxALIGN_CENTER | wxALL, 5);

	WxPanel5 = new wxPanel(WxPanel2, ID_WXPANEL5, wxPoint(115, 5), wxSize(144, 36));
	WxBoxSizer2->Add(WxPanel5, 0, wxALIGN_CENTER | wxEXPAND | wxALL, 5);

	WxBoxSizer4 = new wxBoxSizer(wxHORIZONTAL);
	WxPanel5->SetSizer(WxBoxSizer4);
	WxPanel5->SetAutoLayout(true);

	wxIntegerValidator<unsigned long>
		decimalEditValidator(NULL, wxNUM_VAL_DEFAULT);
	decimalEditValidator.SetMax(4294967295);

	WxDecimalEdit = new wxTextCtrl(WxPanel5, ID_WXDECIMALEDIT, _("0"), wxPoint(5, 5), wxSize(129, 24), 0, decimalEditValidator, _("WxDecimalEdit"));
	WxBoxSizer4->Add(WxDecimalEdit, 0, wxALIGN_CENTER | wxALL, 5);

	WxBoxSizer2->AddStretchSpacer(1);

	WxPanel6 = new wxPanel(WxPanel2, ID_WXPANEL6, wxPoint(269, 5), wxSize(295, 36));
	WxBoxSizer2->Add(WxPanel6, 1, wxALIGN_RIGHT | wxEXPAND | wxALL, 5);

	WxBoxSizer5 = new wxBoxSizer(wxHORIZONTAL);
	WxPanel6->SetSizer(WxBoxSizer5);
	WxPanel6->SetAutoLayout(true);

	WxAllOnButton = new wxButton(WxPanel6, ID_WXALLONBUTTON, _("All On"), wxPoint(5, 5), wxSize(87, 30), 0, wxDefaultValidator, _("WxAllOnButton"));
	WxBoxSizer5->Add(WxAllOnButton, 0, wxALIGN_RIGHT | wxEXPAND | wxALL, 5);

	WxAllOffButton = new wxButton(WxPanel6, ID_WXALLOFFBUTTON, _("All Off"), wxPoint(102, 5), wxSize(87, 30), 0, wxDefaultValidator, _("WxAllOffButton"));
	WxBoxSizer5->Add(WxAllOffButton, 0, wxALIGN_RIGHT | wxEXPAND | wxALL, 5);

	WxExitButton = new wxButton(WxPanel6, ID_WXEXITBUTTON, _("Exit"), wxPoint(199, 5), wxSize(87, 30), 0, wxDefaultValidator, _("WxExitButton"));
	WxBoxSizer5->Add(WxExitButton, 0, wxALIGN_RIGHT | wxEXPAND | wxALL, 5);

	WxMenuBar1 = new wxMenuBar();
	wxMenu *ID_MNU_FILE_1005_Mnu_Obj = new wxMenu();
	ID_MNU_FILE_1005_Mnu_Obj->Append(ID_MNU_LOADPROFILE_1195, _("&Load Profile"), _(""), wxITEM_NORMAL);
	ID_MNU_FILE_1005_Mnu_Obj->Append(ID_MNU_UNLOADPROFILE_1198, _("&Unload Profile"), _(""), wxITEM_NORMAL);
	ID_MNU_FILE_1005_Mnu_Obj->AppendSeparator();
	ID_MNU_FILE_1005_Mnu_Obj->AppendCheckItem(ID_MNU_LOADLASTPROFILE, _("Load Last &Profile On Startup"));
	wxMenu* menuRecent = new wxMenu;
	ID_MNU_FILE_1005_Mnu_Obj->AppendSubMenu(menuRecent, _("&Recent Profiles"));

	m_fileHistory = new wxFileHistory(9,wxID_FILE1);
	m_fileHistory->UseMenu(menuRecent);
	m_fileConfig = new wxFileConfig("BitMaker");
	wxConfigBase::Set( m_fileConfig );
	m_fileConfig->SetPath(_("/RecentFiles"));
	m_fileHistory->Load(*m_fileConfig);
	m_fileConfig->SetPath(_(".."));
	ID_MNU_FILE_1005_Mnu_Obj->AppendSeparator();
	ID_MNU_FILE_1005_Mnu_Obj->Append(ID_MNU_EXIT_1009, _("E&xit\tAlt-F4"), _(""), wxITEM_NORMAL);
	WxMenuBar1->Append(ID_MNU_FILE_1005_Mnu_Obj, _("&File"));

	wxMenu *ID_MNU_HELP_1006_Mnu_Obj = new wxMenu();
	ID_MNU_HELP_1006_Mnu_Obj->Append(ID_MNU_CHECKFORUPDATES_1203, _("&Check For Updates"), _(""), wxITEM_NORMAL);
	ID_MNU_HELP_1006_Mnu_Obj->AppendSeparator();
	ID_MNU_HELP_1006_Mnu_Obj->Append(ID_MNU_WEBSITE_1007, _("&Website"), _(""), wxITEM_NORMAL);
	ID_MNU_HELP_1006_Mnu_Obj->Append(ID_MNU_ABOUT_1008, _("&About"), _(""), wxITEM_NORMAL);
	WxMenuBar1->Append(ID_MNU_HELP_1006_Mnu_Obj, _("&Help"));
	SetMenuBar(WxMenuBar1);

	SetTitle(_("BitMaker"));
	SetIcon(wxNullIcon);

	Layout();
	GetSizer()->Fit(this);
	GetSizer()->SetSizeHints(this);
	Center();

	////GUI Items Creation End

	WxBit0->SetCursor(wxCURSOR_HAND);
	WxBit0->Connect(wxID_ANY, wxEVT_LEFT_DOWN, wxMouseEventHandler(BitMakerFrm::OnClickBit), NULL, this);
	WxBit0Label->ToggleWindowStyle(wxST_ELLIPSIZE_END);
	WxBit0Label->Connect(wxID_ANY, wxEVT_LEFT_DOWN, wxMouseEventHandler(BitMakerFrm::OnClickBit), NULL, this);
	WxBit1->SetCursor(wxCURSOR_HAND);
	WxBit1->Connect(wxID_ANY, wxEVT_LEFT_DOWN, wxMouseEventHandler(BitMakerFrm::OnClickBit), NULL, this);
	WxBit1Label->ToggleWindowStyle(wxST_ELLIPSIZE_END);
	WxBit1Label->Connect(wxID_ANY, wxEVT_LEFT_DOWN, wxMouseEventHandler(BitMakerFrm::OnClickBit), NULL, this);
	WxBit2->SetCursor(wxCURSOR_HAND);
	WxBit2->Connect(wxID_ANY, wxEVT_LEFT_DOWN, wxMouseEventHandler(BitMakerFrm::OnClickBit), NULL, this);
	WxBit2Label->ToggleWindowStyle(wxST_ELLIPSIZE_END);
	WxBit2Label->Connect(wxID_ANY, wxEVT_LEFT_DOWN, wxMouseEventHandler(BitMakerFrm::OnClickBit), NULL, this);
	WxBit3->SetCursor(wxCURSOR_HAND);
	WxBit3->Connect(wxID_ANY, wxEVT_LEFT_DOWN, wxMouseEventHandler(BitMakerFrm::OnClickBit), NULL, this);
	WxBit3Label->ToggleWindowStyle(wxST_ELLIPSIZE_END);
	WxBit3Label->Connect(wxID_ANY, wxEVT_LEFT_DOWN, wxMouseEventHandler(BitMakerFrm::OnClickBit), NULL, this);
	WxBit4->SetCursor(wxCURSOR_HAND);
	WxBit4->Connect(wxID_ANY, wxEVT_LEFT_DOWN, wxMouseEventHandler(BitMakerFrm::OnClickBit), NULL, this);
	WxBit4Label->ToggleWindowStyle(wxST_ELLIPSIZE_END);
	WxBit4Label->Connect(wxID_ANY, wxEVT_LEFT_DOWN, wxMouseEventHandler(BitMakerFrm::OnClickBit), NULL, this);
	WxBit5->SetCursor(wxCURSOR_HAND);
	WxBit5->Connect(wxID_ANY, wxEVT_LEFT_DOWN, wxMouseEventHandler(BitMakerFrm::OnClickBit), NULL, this);
	WxBit5Label->ToggleWindowStyle(wxST_ELLIPSIZE_END);
	WxBit5Label->Connect(wxID_ANY, wxEVT_LEFT_DOWN, wxMouseEventHandler(BitMakerFrm::OnClickBit), NULL, this);
	WxBit6->SetCursor(wxCURSOR_HAND);
	WxBit6->Connect(wxID_ANY, wxEVT_LEFT_DOWN, wxMouseEventHandler(BitMakerFrm::OnClickBit), NULL, this);
	WxBit6Label->ToggleWindowStyle(wxST_ELLIPSIZE_END);
	WxBit6Label->Connect(wxID_ANY, wxEVT_LEFT_DOWN, wxMouseEventHandler(BitMakerFrm::OnClickBit), NULL, this);
	WxBit7->SetCursor(wxCURSOR_HAND);
	WxBit7->Connect(wxID_ANY, wxEVT_LEFT_DOWN, wxMouseEventHandler(BitMakerFrm::OnClickBit), NULL, this);
	WxBit7Label->ToggleWindowStyle(wxST_ELLIPSIZE_END);
	WxBit7Label->Connect(wxID_ANY, wxEVT_LEFT_DOWN, wxMouseEventHandler(BitMakerFrm::OnClickBit), NULL, this);
	WxBit8->SetCursor(wxCURSOR_HAND);
	WxBit8->Connect(wxID_ANY, wxEVT_LEFT_DOWN, wxMouseEventHandler(BitMakerFrm::OnClickBit), NULL, this);
	WxBit8Label->ToggleWindowStyle(wxST_ELLIPSIZE_END);
	WxBit8Label->Connect(wxID_ANY, wxEVT_LEFT_DOWN, wxMouseEventHandler(BitMakerFrm::OnClickBit), NULL, this);
	WxBit9->SetCursor(wxCURSOR_HAND);
	WxBit9->Connect(wxID_ANY, wxEVT_LEFT_DOWN, wxMouseEventHandler(BitMakerFrm::OnClickBit), NULL, this);
	WxBit9Label->ToggleWindowStyle(wxST_ELLIPSIZE_END);
	WxBit9Label->Connect(wxID_ANY, wxEVT_LEFT_DOWN, wxMouseEventHandler(BitMakerFrm::OnClickBit), NULL, this);
	WxBit10->SetCursor(wxCURSOR_HAND);
	WxBit10->Connect(wxID_ANY, wxEVT_LEFT_DOWN, wxMouseEventHandler(BitMakerFrm::OnClickBit), NULL, this);
	WxBit10Label->ToggleWindowStyle(wxST_ELLIPSIZE_END);
	WxBit10Label->Connect(wxID_ANY, wxEVT_LEFT_DOWN, wxMouseEventHandler(BitMakerFrm::OnClickBit), NULL, this);
	WxBit11->SetCursor(wxCURSOR_HAND);
	WxBit11->Connect(wxID_ANY, wxEVT_LEFT_DOWN, wxMouseEventHandler(BitMakerFrm::OnClickBit), NULL, this);
	WxBit11Label->ToggleWindowStyle(wxST_ELLIPSIZE_END);
	WxBit11Label->Connect(wxID_ANY, wxEVT_LEFT_DOWN, wxMouseEventHandler(BitMakerFrm::OnClickBit), NULL, this);
	WxBit12->SetCursor(wxCURSOR_HAND);
	WxBit12->Connect(wxID_ANY, wxEVT_LEFT_DOWN, wxMouseEventHandler(BitMakerFrm::OnClickBit), NULL, this);
	WxBit12Label->ToggleWindowStyle(wxST_ELLIPSIZE_END);
	WxBit12Label->Connect(wxID_ANY, wxEVT_LEFT_DOWN, wxMouseEventHandler(BitMakerFrm::OnClickBit), NULL, this);
	WxBit13->SetCursor(wxCURSOR_HAND);
	WxBit13->Connect(wxID_ANY, wxEVT_LEFT_DOWN, wxMouseEventHandler(BitMakerFrm::OnClickBit), NULL, this);
	WxBit13Label->ToggleWindowStyle(wxST_ELLIPSIZE_END);
	WxBit13Label->Connect(wxID_ANY, wxEVT_LEFT_DOWN, wxMouseEventHandler(BitMakerFrm::OnClickBit), NULL, this);
	WxBit14->SetCursor(wxCURSOR_HAND);
	WxBit14->Connect(wxID_ANY, wxEVT_LEFT_DOWN, wxMouseEventHandler(BitMakerFrm::OnClickBit), NULL, this);
	WxBit14Label->ToggleWindowStyle(wxST_ELLIPSIZE_END);
	WxBit14Label->Connect(wxID_ANY, wxEVT_LEFT_DOWN, wxMouseEventHandler(BitMakerFrm::OnClickBit), NULL, this);
	WxBit15->SetCursor(wxCURSOR_HAND);
	WxBit15->Connect(wxID_ANY, wxEVT_LEFT_DOWN, wxMouseEventHandler(BitMakerFrm::OnClickBit), NULL, this);
	WxBit15Label->ToggleWindowStyle(wxST_ELLIPSIZE_END);
	WxBit15Label->Connect(wxID_ANY, wxEVT_LEFT_DOWN, wxMouseEventHandler(BitMakerFrm::OnClickBit), NULL, this);
	WxBit16->SetCursor(wxCURSOR_HAND);
	WxBit16->Connect(wxID_ANY, wxEVT_LEFT_DOWN, wxMouseEventHandler(BitMakerFrm::OnClickBit), NULL, this);
	WxBit16Label->ToggleWindowStyle(wxST_ELLIPSIZE_END);
	WxBit16Label->Connect(wxID_ANY, wxEVT_LEFT_DOWN, wxMouseEventHandler(BitMakerFrm::OnClickBit), NULL, this);
	WxBit17->SetCursor(wxCURSOR_HAND);
	WxBit17->Connect(wxID_ANY, wxEVT_LEFT_DOWN, wxMouseEventHandler(BitMakerFrm::OnClickBit), NULL, this);
	WxBit17Label->ToggleWindowStyle(wxST_ELLIPSIZE_END);
	WxBit17Label->Connect(wxID_ANY, wxEVT_LEFT_DOWN, wxMouseEventHandler(BitMakerFrm::OnClickBit), NULL, this);
	WxBit18->SetCursor(wxCURSOR_HAND);
	WxBit18->Connect(wxID_ANY, wxEVT_LEFT_DOWN, wxMouseEventHandler(BitMakerFrm::OnClickBit), NULL, this);
	WxBit18Label->ToggleWindowStyle(wxST_ELLIPSIZE_END);
	WxBit18Label->Connect(wxID_ANY, wxEVT_LEFT_DOWN, wxMouseEventHandler(BitMakerFrm::OnClickBit), NULL, this);
	WxBit19->SetCursor(wxCURSOR_HAND);
	WxBit19->Connect(wxID_ANY, wxEVT_LEFT_DOWN, wxMouseEventHandler(BitMakerFrm::OnClickBit), NULL, this);
	WxBit19Label->ToggleWindowStyle(wxST_ELLIPSIZE_END);
	WxBit19Label->Connect(wxID_ANY, wxEVT_LEFT_DOWN, wxMouseEventHandler(BitMakerFrm::OnClickBit), NULL, this);
	WxBit20->SetCursor(wxCURSOR_HAND);
	WxBit20->Connect(wxID_ANY, wxEVT_LEFT_DOWN, wxMouseEventHandler(BitMakerFrm::OnClickBit), NULL, this);
	WxBit20Label->ToggleWindowStyle(wxST_ELLIPSIZE_END);
	WxBit20Label->Connect(wxID_ANY, wxEVT_LEFT_DOWN, wxMouseEventHandler(BitMakerFrm::OnClickBit), NULL, this);
	WxBit21->SetCursor(wxCURSOR_HAND);
	WxBit21->Connect(wxID_ANY, wxEVT_LEFT_DOWN, wxMouseEventHandler(BitMakerFrm::OnClickBit), NULL, this);
	WxBit21Label->ToggleWindowStyle(wxST_ELLIPSIZE_END);
	WxBit21Label->Connect(wxID_ANY, wxEVT_LEFT_DOWN, wxMouseEventHandler(BitMakerFrm::OnClickBit), NULL, this);
	WxBit22->SetCursor(wxCURSOR_HAND);
	WxBit22->Connect(wxID_ANY, wxEVT_LEFT_DOWN, wxMouseEventHandler(BitMakerFrm::OnClickBit), NULL, this);
	WxBit22Label->ToggleWindowStyle(wxST_ELLIPSIZE_END);
	WxBit22Label->Connect(wxID_ANY, wxEVT_LEFT_DOWN, wxMouseEventHandler(BitMakerFrm::OnClickBit), NULL, this);
	WxBit23->SetCursor(wxCURSOR_HAND);
	WxBit23->Connect(wxID_ANY, wxEVT_LEFT_DOWN, wxMouseEventHandler(BitMakerFrm::OnClickBit), NULL, this);
	WxBit23Label->ToggleWindowStyle(wxST_ELLIPSIZE_END);
	WxBit23Label->Connect(wxID_ANY, wxEVT_LEFT_DOWN, wxMouseEventHandler(BitMakerFrm::OnClickBit), NULL, this);
	WxBit24->SetCursor(wxCURSOR_HAND);
	WxBit24->Connect(wxID_ANY, wxEVT_LEFT_DOWN, wxMouseEventHandler(BitMakerFrm::OnClickBit), NULL, this);
	WxBit24Label->ToggleWindowStyle(wxST_ELLIPSIZE_END);
	WxBit24Label->Connect(wxID_ANY, wxEVT_LEFT_DOWN, wxMouseEventHandler(BitMakerFrm::OnClickBit), NULL, this);
	WxBit25->SetCursor(wxCURSOR_HAND);
	WxBit25->Connect(wxID_ANY, wxEVT_LEFT_DOWN, wxMouseEventHandler(BitMakerFrm::OnClickBit), NULL, this);
	WxBit25Label->ToggleWindowStyle(wxST_ELLIPSIZE_END);
	WxBit25Label->Connect(wxID_ANY, wxEVT_LEFT_DOWN, wxMouseEventHandler(BitMakerFrm::OnClickBit), NULL, this);
	WxBit26->SetCursor(wxCURSOR_HAND);
	WxBit26->Connect(wxID_ANY, wxEVT_LEFT_DOWN, wxMouseEventHandler(BitMakerFrm::OnClickBit), NULL, this);
	WxBit26Label->ToggleWindowStyle(wxST_ELLIPSIZE_END);
	WxBit26Label->Connect(wxID_ANY, wxEVT_LEFT_DOWN, wxMouseEventHandler(BitMakerFrm::OnClickBit), NULL, this);
	WxBit27->SetCursor(wxCURSOR_HAND);
	WxBit27->Connect(wxID_ANY, wxEVT_LEFT_DOWN, wxMouseEventHandler(BitMakerFrm::OnClickBit), NULL, this);
	WxBit27Label->ToggleWindowStyle(wxST_ELLIPSIZE_END);
	WxBit27Label->Connect(wxID_ANY, wxEVT_LEFT_DOWN, wxMouseEventHandler(BitMakerFrm::OnClickBit), NULL, this);
	WxBit28->SetCursor(wxCURSOR_HAND);
	WxBit28->Connect(wxID_ANY, wxEVT_LEFT_DOWN, wxMouseEventHandler(BitMakerFrm::OnClickBit), NULL, this);
	WxBit28Label->ToggleWindowStyle(wxST_ELLIPSIZE_END);
	WxBit28Label->Connect(wxID_ANY, wxEVT_LEFT_DOWN, wxMouseEventHandler(BitMakerFrm::OnClickBit), NULL, this);
	WxBit29->SetCursor(wxCURSOR_HAND);
	WxBit29->Connect(wxID_ANY, wxEVT_LEFT_DOWN, wxMouseEventHandler(BitMakerFrm::OnClickBit), NULL, this);
	WxBit29Label->ToggleWindowStyle(wxST_ELLIPSIZE_END);
	WxBit29Label->Connect(wxID_ANY, wxEVT_LEFT_DOWN, wxMouseEventHandler(BitMakerFrm::OnClickBit), NULL, this);
	WxBit30->SetCursor(wxCURSOR_HAND);
	WxBit30->Connect(wxID_ANY, wxEVT_LEFT_DOWN, wxMouseEventHandler(BitMakerFrm::OnClickBit), NULL, this);
	WxBit30Label->ToggleWindowStyle(wxST_ELLIPSIZE_END);
	WxBit30Label->Connect(wxID_ANY, wxEVT_LEFT_DOWN, wxMouseEventHandler(BitMakerFrm::OnClickBit), NULL, this);
	WxBit31->SetCursor(wxCURSOR_HAND);
	WxBit31->Connect(wxID_ANY, wxEVT_LEFT_DOWN, wxMouseEventHandler(BitMakerFrm::OnClickBit), NULL, this);
	WxBit31Label->ToggleWindowStyle(wxST_ELLIPSIZE_END);
	WxBit31Label->Connect(wxID_ANY, wxEVT_LEFT_DOWN, wxMouseEventHandler(BitMakerFrm::OnClickBit), NULL, this);

	SetBackgroundColour(wxT("LIGHTGRAY"));
	WxDecimalEdit->SetFocus();

}

void BitMakerFrm::OnClose(wxCloseEvent& event)
{
	m_fileConfig->SetPath(wxT("/RecentFiles"));
	m_fileHistory->Save(*m_fileConfig);
	delete m_fileConfig;
	delete m_fileHistory;
	Destroy();
}


/*
 * Mnuloadprofile1195Click
 */
void BitMakerFrm::Mnuloadprofile1195Click(wxCommandEvent& event)
{
	wxFileDialog openFileDialog(this, wxT("Select a profile"), profilePath, wxT(""), wxT("BitMaker profile (*.bmpr)|*.bmpr"), wxFD_OPEN|wxFD_FILE_MUST_EXIST);

	if (openFileDialog.ShowModal() == wxID_CANCEL)
	{
		return;
	}
	loadProfile(openFileDialog.GetPath());

	openFileDialog.Destroy();
}


void BitMakerFrm::removeFromHistory(wxString filePath)
{
	int i;
	int iMax = m_fileHistory->GetCount();

	for(i = 0; i < iMax; i++)
	{
		if(m_fileHistory->GetHistoryFile(i) == filePath)
		{
			m_fileHistory->RemoveFileFromHistory(i);
			return;
		}
	}
}


void BitMakerFrm::loadProfile(wxString filePath, bool isStartup)
{
	wxTextFile *file = new wxTextFile(filePath);

	if(!file->Exists())
	{
		if(!isStartup)
		{
			bool bRemoveMissingProfile = false;
			m_fileConfig->SetPath("/Settings");
			m_fileConfig->Read("RemoveMissingProfiles", &bRemoveMissingProfile);

			if(bRemoveMissingProfile)
			{
				wxMessageBox(wxT("Unable find the selected profile."));
				removeFromHistory(filePath);
			}
			else
			{
				int result = wxID_NO;

				wxRichMessageDialog dlg(this, wxT("Unable find the selected profile.\nWould you like to remove this profile from the Recent Profiles list?\n"), wxT("Cannot find profile"), wxYES_NO);
				dlg.ShowCheckBox("Always perform this action");
				result = dlg.ShowModal();

				bool bRememberRemove = false;

				if(result == wxID_YES)
				{
					removeFromHistory(filePath);
					bRememberRemove = true;
				}

				if(dlg.IsCheckBoxChecked())
				{
					m_fileConfig->Write(wxT("RemoveMissingProfiles"), bRememberRemove);
				}
			}
			m_fileConfig->SetPath("/");
		}

		return;
	}

	if(!file->Open())
	{
		wxMessageBox(wxT("Unable to open profile."));
		return;
	}

	wxStaticText* targetLabel;
	wxTextCtrl* targetBit;
	bool validLabel = false;
	wxString line;
	wxString value;
	int currentBit;
	resetBitLabels();

	for(line = file->GetFirstLine(); !file->Eof(); line = file->GetNextLine())
	{
		// Comment, so skip it.
		if(line.StartsWith(wxT("#")))
		{
			continue;
		}
		wxString lineEnd;
		if(line.StartsWith(wxT("[Bit"), &lineEnd))
		{
			if(lineEnd.EndsWith(wxT("]"), &value))
			{
				currentBit = wxAtoi(value);
				switch(currentBit)
				{
					case 0:
						targetLabel = WxBit0Label;
						targetBit = WxBit0;
						validLabel = true;
						break;
					case 1:
						targetLabel = WxBit1Label;
						targetBit = WxBit1;
						validLabel = true;
						break;
					case 2:
						targetLabel = WxBit2Label;
						targetBit = WxBit2;
						validLabel = true;
						break;
					case 3:
						targetLabel = WxBit3Label;
						targetBit = WxBit3;
						validLabel = true;
						break;
					case 4:
						targetLabel = WxBit4Label;
						targetBit = WxBit4;
						validLabel = true;
						break;
					case 5:
						targetLabel = WxBit5Label;
						targetBit = WxBit5;
						validLabel = true;
						break;
					case 6:
						targetLabel = WxBit6Label;
						targetBit = WxBit6;
						validLabel = true;
						break;
					case 7:
						targetLabel = WxBit7Label;
						targetBit = WxBit7;
						validLabel = true;
						break;
					case 8:
						targetLabel = WxBit8Label;
						targetBit = WxBit8;
						validLabel = true;
						break;
					case 9:
						targetLabel = WxBit9Label;
						targetBit = WxBit9;
						validLabel = true;
						break;
					case 10:
						targetLabel = WxBit10Label;
						targetBit = WxBit10;
						validLabel = true;
						break;
					case 11:
						targetLabel = WxBit11Label;
						targetBit = WxBit11;
						validLabel = true;
						break;
					case 12:
						targetLabel = WxBit12Label;
						targetBit = WxBit12;
						validLabel = true;
						break;
					case 13:
						targetLabel = WxBit13Label;
						targetBit = WxBit13;
						validLabel = true;
						break;
					case 14:
						targetLabel = WxBit14Label;
						targetBit = WxBit14;
						validLabel = true;
						break;
					case 15:
						targetLabel = WxBit15Label;
						targetBit = WxBit15;
						validLabel = true;
						break;
					case 16:
						targetLabel = WxBit16Label;
						targetBit = WxBit16;
						validLabel = true;
						break;
					case 17:
						targetLabel = WxBit17Label;
						targetBit = WxBit17;
						validLabel = true;
						break;
					case 18:
						targetLabel = WxBit18Label;
						targetBit = WxBit18;
						validLabel = true;
						break;
					case 19:
						targetLabel = WxBit19Label;
						targetBit = WxBit19;
						validLabel = true;
						break;
					case 20:
						targetLabel = WxBit20Label;
						targetBit = WxBit20;
						validLabel = true;
						break;
					case 21:
						targetLabel = WxBit21Label;
						targetBit = WxBit21;
						validLabel = true;
						break;
					case 22:
						targetLabel = WxBit22Label;
						targetBit = WxBit22;
						validLabel = true;
						break;
					case 23:
						targetLabel = WxBit23Label;
						targetBit = WxBit23;
						validLabel = true;
						break;
					case 24:
						targetLabel = WxBit24Label;
						targetBit = WxBit24;
						validLabel = true;
						break;
					case 25:
						targetLabel = WxBit25Label;
						targetBit = WxBit25;
						validLabel = true;
						break;
					case 26:
						targetLabel = WxBit26Label;
						targetBit = WxBit26;
						validLabel = true;
						break;
					case 27:
						targetLabel = WxBit27Label;
						targetBit = WxBit27;
						validLabel = true;
						break;
					case 28:
						targetLabel = WxBit28Label;
						targetBit = WxBit28;
						validLabel = true;
						break;
					case 29:
						targetLabel = WxBit29Label;
						targetBit = WxBit29;
						validLabel = true;
						break;
					case 30:
						targetLabel = WxBit30Label;
						targetBit = WxBit30;
						validLabel = true;
						break;
					case 31:
						targetLabel = WxBit31Label;
						targetBit = WxBit31;
						validLabel = true;
						break;
				}
			}
		}
		else if(line.StartsWith(wxT("Enabled="), &value) && validLabel)
		{
			wxSizer* targetSizer = targetBit->GetContainingSizer();
			if(value.MakeLower() == wxT("true"))
			{
				targetSizer->Show(true);
			}
			else
			{
				targetBit->SetValue(wxT("0"));
				targetSizer->Show(false);
			}
			targetSizer->Layout();
		}
		else if(line.StartsWith(wxT("Label="), &value) && validLabel)
		{
			if(value != wxT(""))
			{
				targetLabel->SetLabel(value);
			}
		}
	}
	WxGridSizer2->Layout();
	recalcBitVal();
	m_fileHistory->AddFileToHistory(filePath);

	m_fileConfig->SetPath("/Settings");

	m_fileConfig->Write(wxT("LastUsedProfile"), filePath);

	m_fileConfig->SetPath("/");

}

/*
 * Mnuexit1009Click
 */
void BitMakerFrm::Mnuexit1009Click(wxCommandEvent& event)
{
	Close(true);
}

/*
 * Mnuwebsite1007Click
 */
void BitMakerFrm::Mnuwebsite1007Click(wxCommandEvent& event)
{
	wxLaunchDefaultBrowser(wxT("http://firedancer-software.com/software/bitmaker/"));
}

/*
 * Mnuabout1008Click
 */
void BitMakerFrm::Mnuabout1008Click(wxCommandEvent& event)
{
	wxAboutDialogInfo info;
	info.SetName(PRODUCT_NAME);
    info.SetVersion(PRODUCT_VERSION);
    info.SetCopyright(_T("(C) 2013 Firedancer Software.\nLicenced under the GPL Version 3.\n"));
    info.SetDescription(wxT("An interface for manipulating bitwise decimal numbers."));
	wxAboutBox(info);
}


/*
 * WxExitButtonClick
 */
void BitMakerFrm::WxExitButtonClick(wxCommandEvent& event)
{
	Close(true);
}


/*
 * WxCalcButtonClick
 */
void BitMakerFrm::WxCalcButtonClick(wxCommandEvent& event)
{
	wxString tmp = WxDecimalEdit->GetValue();

	if(wxIsEmpty(tmp))
	{
		WxDecimalEdit->SetFocus();
		WxDecimalEdit->SetValue(wxT("0"));
		tmp = wxT("0");
		return;
	}

	unsigned long val;

	if(!tmp.ToULong(&val))
	{
		WxDecimalEdit->SetFocus();
		return;
	}


    if(val & (1 << 0))
    {
        WxBit0->SetFont(boldFont);
        WxBit0->ChangeValue(wxT("1"));
    }
    else
    {
        WxBit0->SetFont(normalFont);
        WxBit0->ChangeValue(wxT("0"));
    }

    if(val & (1 << 1))
    {
        WxBit1->SetFont(boldFont);
        WxBit1->ChangeValue(wxT("1"));
    }
    else
    {
        WxBit1->SetFont(normalFont);
        WxBit1->ChangeValue(wxT("0"));
    }

    if(val & (1 << 2))
    {
        WxBit2->SetFont(boldFont);
        WxBit2->ChangeValue(wxT("1"));
    }
    else
    {
        WxBit2->SetFont(normalFont);
        WxBit2->ChangeValue(wxT("0"));
    }

    if(val & (1 << 3))
    {
        WxBit3->SetFont(boldFont);
        WxBit3->ChangeValue(wxT("1"));
    }
    else
    {
        WxBit3->SetFont(normalFont);
        WxBit3->ChangeValue(wxT("0"));
    }

    if(val & (1 << 4))
    {
        WxBit4->SetFont(boldFont);
        WxBit4->ChangeValue(wxT("1"));
    }
    else
    {
        WxBit4->SetFont(normalFont);
        WxBit4->ChangeValue(wxT("0"));
    }

    if(val & (1 << 5))
    {
        WxBit5->SetFont(boldFont);
        WxBit5->ChangeValue(wxT("1"));
    }
    else
    {
        WxBit5->SetFont(normalFont);
        WxBit5->ChangeValue(wxT("0"));
    }

    if(val & (1 << 6))
    {
        WxBit6->SetFont(boldFont);
        WxBit6->ChangeValue(wxT("1"));
    }
    else
    {
        WxBit6->SetFont(normalFont);
        WxBit6->ChangeValue(wxT("0"));
    }

    if(val & (1 << 7))
    {
        WxBit7->SetFont(boldFont);
        WxBit7->ChangeValue(wxT("1"));
    }
    else
    {
        WxBit7->SetFont(normalFont);
        WxBit7->ChangeValue(wxT("0"));
    }

    if(val & (1 << 8))
    {
        WxBit8->SetFont(boldFont);
        WxBit8->ChangeValue(wxT("1"));
    }
    else
    {
        WxBit8->SetFont(normalFont);
        WxBit8->ChangeValue(wxT("0"));
    }

    if(val & (1 << 9))
    {
        WxBit9->SetFont(boldFont);
        WxBit9->ChangeValue(wxT("1"));
    }
    else
    {
        WxBit9->SetFont(normalFont);
        WxBit9->ChangeValue(wxT("0"));
    }

    if(val & (1 << 10))
    {
        WxBit10->SetFont(boldFont);
        WxBit10->ChangeValue(wxT("1"));
    }
    else
    {
        WxBit10->SetFont(normalFont);
        WxBit10->ChangeValue(wxT("0"));
    }

    if(val & (1 << 11))
    {
        WxBit11->SetFont(boldFont);
        WxBit11->ChangeValue(wxT("1"));
    }
    else
    {
        WxBit11->SetFont(normalFont);
        WxBit11->ChangeValue(wxT("0"));
    }

    if(val & (1 << 12))
    {
        WxBit12->SetFont(boldFont);
        WxBit12->ChangeValue(wxT("1"));
    }
    else
    {
        WxBit12->SetFont(normalFont);
        WxBit12->ChangeValue(wxT("0"));
    }

    if(val & (1 << 13))
    {
        WxBit13->SetFont(boldFont);
        WxBit13->ChangeValue(wxT("1"));
    }
    else
    {
        WxBit13->SetFont(normalFont);
        WxBit13->ChangeValue(wxT("0"));
    }

    if(val & (1 << 14))
    {
        WxBit14->SetFont(boldFont);
        WxBit14->ChangeValue(wxT("1"));
    }
    else
    {
        WxBit14->SetFont(normalFont);
        WxBit14->ChangeValue(wxT("0"));
    }

    if(val & (1 << 15))
    {
        WxBit15->SetFont(boldFont);
        WxBit15->ChangeValue(wxT("1"));
    }
    else
    {
        WxBit15->SetFont(normalFont);
        WxBit15->ChangeValue(wxT("0"));
    }

    if(val & (1 << 16))
    {
        WxBit16->SetFont(boldFont);
        WxBit16->ChangeValue(wxT("1"));
    }
    else
    {
        WxBit16->SetFont(normalFont);
        WxBit16->ChangeValue(wxT("0"));
    }

    if(val & (1 << 17))
    {
        WxBit17->SetFont(boldFont);
        WxBit17->ChangeValue(wxT("1"));
    }
    else
    {
        WxBit17->SetFont(normalFont);
        WxBit17->ChangeValue(wxT("0"));
    }

    if(val & (1 << 18))
    {
        WxBit18->SetFont(boldFont);
        WxBit18->ChangeValue(wxT("1"));
    }
    else
    {
        WxBit18->SetFont(normalFont);
        WxBit18->ChangeValue(wxT("0"));
    }

    if(val & (1 << 19))
    {
        WxBit19->SetFont(boldFont);
        WxBit19->ChangeValue(wxT("1"));
    }
    else
    {
        WxBit19->SetFont(normalFont);
        WxBit19->ChangeValue(wxT("0"));
    }

    if(val & (1 << 20))
    {
        WxBit20->SetFont(boldFont);
        WxBit20->ChangeValue(wxT("1"));
    }
    else
    {
        WxBit20->SetFont(normalFont);
        WxBit20->ChangeValue(wxT("0"));
    }

    if(val & (1 << 21))
    {
        WxBit21->SetFont(boldFont);
        WxBit21->ChangeValue(wxT("1"));
    }
    else
    {
        WxBit21->SetFont(normalFont);
        WxBit21->ChangeValue(wxT("0"));
    }

    if(val & (1 << 22))
    {
        WxBit22->SetFont(boldFont);
        WxBit22->ChangeValue(wxT("1"));
    }
    else
    {
        WxBit22->SetFont(normalFont);
        WxBit22->ChangeValue(wxT("0"));
    }

    if(val & (1 << 23))
    {
        WxBit23->SetFont(boldFont);
        WxBit23->ChangeValue(wxT("1"));
    }
    else
    {
        WxBit23->SetFont(normalFont);
        WxBit23->ChangeValue(wxT("0"));
    }

    if(val & (1 << 24))
    {
        WxBit24->SetFont(boldFont);
        WxBit24->ChangeValue(wxT("1"));
    }
    else
    {
        WxBit24->SetFont(normalFont);
        WxBit24->ChangeValue(wxT("0"));
    }

    if(val & (1 << 25))
    {
        WxBit25->SetFont(boldFont);
        WxBit25->ChangeValue(wxT("1"));
    }
    else
    {
        WxBit25->SetFont(normalFont);
        WxBit25->ChangeValue(wxT("0"));
    }

    if(val & (1 << 26))
    {
        WxBit26->SetFont(boldFont);
        WxBit26->ChangeValue(wxT("1"));
    }
    else
    {
        WxBit26->SetFont(normalFont);
        WxBit26->ChangeValue(wxT("0"));
    }

    if(val & (1 << 27))
    {
        WxBit27->SetFont(boldFont);
        WxBit27->ChangeValue(wxT("1"));
    }
    else
    {
        WxBit27->SetFont(normalFont);
        WxBit27->ChangeValue(wxT("0"));
    }

    if(val & (1 << 28))
    {
        WxBit28->SetFont(boldFont);
        WxBit28->ChangeValue(wxT("1"));
    }
    else
    {
        WxBit28->SetFont(normalFont);
        WxBit28->ChangeValue(wxT("0"));
    }

    if(val & (1 << 29))
    {
        WxBit29->SetFont(boldFont);
        WxBit29->ChangeValue(wxT("1"));
    }
    else
    {
        WxBit29->SetFont(normalFont);
        WxBit29->ChangeValue(wxT("0"));
    }

   if(val & (1 << 30))
    {
        WxBit30->SetFont(boldFont);
        WxBit30->ChangeValue(wxT("1"));
    }
    else
    {
        WxBit30->SetFont(normalFont);
        WxBit30->ChangeValue(wxT("0"));
    }

    if(val & (1 << 31))
    {
        WxBit31->SetFont(boldFont);
        WxBit31->ChangeValue(wxT("1"));
    }
    else
    {
        WxBit31->SetFont(normalFont);
        WxBit31->ChangeValue(wxT("0"));
    }

}

void BitMakerFrm::OnClickBit(wxMouseEvent& event)
{
	wxString val;
	wxTextCtrl* clickedBox;

	switch(event.GetId())
	{
		case ID_WXBIT0:
		case ID_WXBIT0LABEL:
			clickedBox = WxBit0;
			break;
		case ID_WXBIT1:
		case ID_WXBIT1LABEL:
			clickedBox = WxBit1;
			break;
		case ID_WXBIT2:
		case ID_WXBIT2LABEL:
			clickedBox = WxBit2;
			break;
		case ID_WXBIT3:
		case ID_WXBIT3LABEL:
			clickedBox = WxBit3;
			break;
		case ID_WXBIT4:
		case ID_WXBIT4LABEL:
			clickedBox = WxBit4;
			break;
		case ID_WXBIT5:
		case ID_WXBIT5LABEL:
			clickedBox = WxBit5;
			break;
		case ID_WXBIT6:
		case ID_WXBIT6LABEL:
			clickedBox = WxBit6;
			break;
		case ID_WXBIT7:
		case ID_WXBIT7LABEL:
			clickedBox = WxBit7;
			break;
		case ID_WXBIT8:
		case ID_WXBIT8LABEL:
			clickedBox = WxBit8;
			break;
		case ID_WXBIT9:
		case ID_WXBIT9LABEL:
			clickedBox = WxBit9;
			break;
		case ID_WXBIT10:
		case ID_WXBIT10LABEL:
			clickedBox = WxBit10;
			break;
		case ID_WXBIT11:
		case ID_WXBIT11LABEL:
			clickedBox = WxBit11;
			break;
		case ID_WXBIT12:
		case ID_WXBIT12LABEL:
			clickedBox = WxBit12;
			break;
		case ID_WXBIT13:
		case ID_WXBIT13LABEL:
			clickedBox = WxBit13;
			break;
		case ID_WXBIT14:
		case ID_WXBIT14LABEL:
			clickedBox = WxBit14;
			break;
		case ID_WXBIT15:
		case ID_WXBIT15LABEL:
			clickedBox = WxBit15;
			break;
		case ID_WXBIT16:
		case ID_WXBIT16LABEL:
			clickedBox = WxBit16;
			break;
		case ID_WXBIT17:
		case ID_WXBIT17LABEL:
			clickedBox = WxBit17;
			break;
		case ID_WXBIT18:
		case ID_WXBIT18LABEL:
			clickedBox = WxBit18;
			break;
		case ID_WXBIT19:
		case ID_WXBIT19LABEL:
			clickedBox = WxBit19;
			break;
		case ID_WXBIT20:
		case ID_WXBIT20LABEL:
			clickedBox = WxBit20;
			break;
		case ID_WXBIT21:
		case ID_WXBIT21LABEL:
			clickedBox = WxBit21;
			break;
		case ID_WXBIT22:
		case ID_WXBIT22LABEL:
			clickedBox = WxBit22;
			break;
		case ID_WXBIT23:
		case ID_WXBIT23LABEL:
			clickedBox = WxBit23;
			break;
		case ID_WXBIT24:
		case ID_WXBIT24LABEL:
			clickedBox = WxBit24;
			break;
		case ID_WXBIT25:
		case ID_WXBIT25LABEL:
			clickedBox = WxBit25;
			break;
		case ID_WXBIT26:
		case ID_WXBIT26LABEL:
			clickedBox = WxBit26;
			break;
		case ID_WXBIT27:
		case ID_WXBIT27LABEL:
			clickedBox = WxBit27;
			break;
		case ID_WXBIT28:
		case ID_WXBIT28LABEL:
			clickedBox = WxBit28;
			break;
		case ID_WXBIT29:
		case ID_WXBIT29LABEL:
			clickedBox = WxBit29;
			break;
		case ID_WXBIT30:
		case ID_WXBIT30LABEL:
			clickedBox = WxBit30;
			break;
		case ID_WXBIT31:
		case ID_WXBIT31LABEL:
			clickedBox = WxBit31;
			break;
		default:
			return;
			break;
	}
	val = clickedBox->GetValue();

	if(val.IsSameAs(wxT("0")))
	{
        clickedBox->SetFont(boldFont);
        clickedBox->ChangeValue(wxT("1"));
	}
	else
	{
        clickedBox->SetFont(normalFont);
        clickedBox->ChangeValue(wxT("0"));
	}

	recalcBitVal();
}


void BitMakerFrm::recalcBitVal()
{
	wxString val;
	unsigned long total = 0;

	val = WxBit0->GetValue();

	if(val.IsSameAs(wxT("1")))
	{
		total += 1 << 0;
	}

	val = WxBit1->GetValue();

	if(val.IsSameAs(wxT("1")))
	{
		total += 1 << 1;
	}

	val = WxBit2->GetValue();

	if(val.IsSameAs(wxT("1")))
	{
		total += 1 << 2;
	}

	val = WxBit3->GetValue();

	if(val.IsSameAs(wxT("1")))
	{
		total += 1 << 3;
	}

	val = WxBit4->GetValue();

	if(val.IsSameAs(wxT("1")))
	{
		total += 1 << 4;
	}

	val = WxBit5->GetValue();

	if(val.IsSameAs(wxT("1")))
	{
		total += 1 << 5;
	}

	val = WxBit6->GetValue();

	if(val.IsSameAs(wxT("1")))
	{
		total += 1 << 6;
	}

	val = WxBit7->GetValue();

	if(val.IsSameAs(wxT("1")))
	{
		total += 1 << 7;
	}

	val = WxBit8->GetValue();

	if(val.IsSameAs(wxT("1")))
	{
		total += 1 << 8;
	}

	val = WxBit9->GetValue();

	if(val.IsSameAs(wxT("1")))
	{
		total += 1 << 9;
	}
	val = WxBit10->GetValue();

	if(val.IsSameAs(wxT("1")))
	{
		total += 1 << 10;
	}

	val = WxBit11->GetValue();

	if(val.IsSameAs(wxT("1")))
	{
		total += 1 << 11;
	}

	val = WxBit12->GetValue();

	if(val.IsSameAs(wxT("1")))
	{
		total += 1 << 12;
	}

	val = WxBit13->GetValue();

	if(val.IsSameAs(wxT("1")))
	{
		total += 1 << 13;
	}

	val = WxBit14->GetValue();

	if(val.IsSameAs(wxT("1")))
	{
		total += 1 << 14;
	}

	val = WxBit15->GetValue();

	if(val.IsSameAs(wxT("1")))
	{
		total += 1 << 15;
	}

	val = WxBit16->GetValue();

	if(val.IsSameAs(wxT("1")))
	{
		total += 1 << 16;
	}

	val = WxBit17->GetValue();

	if(val.IsSameAs(wxT("1")))
	{
		total += 1 << 17;
	}

	val = WxBit18->GetValue();

	if(val.IsSameAs(wxT("1")))
	{
		total += 1 << 18;
	}

	val = WxBit19->GetValue();

	if(val.IsSameAs(wxT("1")))
	{
		total += 1 << 19;
	}

	val = WxBit20->GetValue();

	if(val.IsSameAs(wxT("1")))
	{
		total += 1 << 20;
	}

	val = WxBit21->GetValue();

	if(val.IsSameAs(wxT("1")))
	{
		total += 1 << 21;
	}

	val = WxBit22->GetValue();

	if(val.IsSameAs(wxT("1")))
	{
		total += 1 << 22;
	}

	val = WxBit23->GetValue();

	if(val.IsSameAs(wxT("1")))
	{
		total += 1 << 23;
	}

	val = WxBit24->GetValue();

	if(val.IsSameAs(wxT("1")))
	{
		total += 1 << 24;
	}

	val = WxBit25->GetValue();

	if(val.IsSameAs(wxT("1")))
	{
		total += 1 << 25;
	}

	val = WxBit26->GetValue();

	if(val.IsSameAs(wxT("1")))
	{
		total += 1 << 26;
	}

	val = WxBit27->GetValue();

	if(val.IsSameAs(wxT("1")))
	{
		total += 1 << 27;
	}

	val = WxBit28->GetValue();

	if(val.IsSameAs(wxT("1")))
	{
		total += 1 << 28;
	}

	val = WxBit29->GetValue();

	if(val.IsSameAs(wxT("1")))
	{
		total += 1 << 29;
	}

	val = WxBit30->GetValue();

	if(val.IsSameAs(wxT("1")))
	{
		total += 1 << 30;
	}

	val = WxBit31->GetValue();

	if(val.IsSameAs(wxT("1")))
	{
		total += 1 << 31;
	}

	wxString final;
	final << total;
	WxDecimalEdit->SetValue(final);
}


void BitMakerFrm::resetBitLabels()
{
	WxBit0Label->SetLabel(wxT("1 << 0"));
	WxBit1Label->SetLabel(wxT("1 << 1"));
	WxBit2Label->SetLabel(wxT("1 << 2"));
	WxBit3Label->SetLabel(wxT("1 << 3"));
	WxBit4Label->SetLabel(wxT("1 << 4"));
	WxBit5Label->SetLabel(wxT("1 << 5"));
	WxBit6Label->SetLabel(wxT("1 << 6"));
	WxBit7Label->SetLabel(wxT("1 << 7"));
	WxBit8Label->SetLabel(wxT("1 << 8"));
	WxBit9Label->SetLabel(wxT("1 << 9"));
	WxBit10Label->SetLabel(wxT("1 << 10"));
	WxBit11Label->SetLabel(wxT("1 << 11"));
	WxBit12Label->SetLabel(wxT("1 << 12"));
	WxBit13Label->SetLabel(wxT("1 << 13"));
	WxBit14Label->SetLabel(wxT("1 << 14"));
	WxBit15Label->SetLabel(wxT("1 << 15"));
	WxBit16Label->SetLabel(wxT("1 << 16"));
	WxBit17Label->SetLabel(wxT("1 << 17"));
	WxBit18Label->SetLabel(wxT("1 << 18"));
	WxBit19Label->SetLabel(wxT("1 << 19"));
	WxBit20Label->SetLabel(wxT("1 << 20"));
	WxBit21Label->SetLabel(wxT("1 << 21"));
	WxBit22Label->SetLabel(wxT("1 << 22"));
	WxBit23Label->SetLabel(wxT("1 << 23"));
	WxBit24Label->SetLabel(wxT("1 << 24"));
	WxBit25Label->SetLabel(wxT("1 << 25"));
	WxBit26Label->SetLabel(wxT("1 << 26"));
	WxBit27Label->SetLabel(wxT("1 << 27"));
	WxBit28Label->SetLabel(wxT("1 << 28"));
	WxBit29Label->SetLabel(wxT("1 << 29"));
	WxBit30Label->SetLabel(wxT("1 << 30"));
	WxBit31Label->SetLabel(wxT("1 << 31"));
}


/*
 * Mnuunloadprofile1198Click
 */
void BitMakerFrm::Mnuunloadprofile1198Click(wxCommandEvent& event)
{
	resetBitLabels();
	WxBoxSizer6->ShowItems(true);
	WxBoxSizer7->ShowItems(true);
	WxBoxSizer8->ShowItems(true);
	WxBoxSizer9->ShowItems(true);
	WxBoxSizer10->ShowItems(true);
	WxBoxSizer11->ShowItems(true);
	WxBoxSizer12->ShowItems(true);
	WxBoxSizer13->ShowItems(true);
	WxBoxSizer14->ShowItems(true);
	WxBoxSizer15->ShowItems(true);
	WxBoxSizer16->ShowItems(true);
	WxBoxSizer17->ShowItems(true);
	WxBoxSizer18->ShowItems(true);
	WxBoxSizer19->ShowItems(true);
	WxBoxSizer20->ShowItems(true);
	WxBoxSizer21->ShowItems(true);
	WxBoxSizer22->ShowItems(true);
	WxBoxSizer23->ShowItems(true);
	WxBoxSizer24->ShowItems(true);
	WxBoxSizer25->ShowItems(true);
	WxBoxSizer26->ShowItems(true);
	WxBoxSizer27->ShowItems(true);
	WxBoxSizer28->ShowItems(true);
	WxBoxSizer29->ShowItems(true);
	WxBoxSizer30->ShowItems(true);
	WxBoxSizer31->ShowItems(true);
	WxBoxSizer32->ShowItems(true);
	WxBoxSizer33->ShowItems(true);
	WxBoxSizer34->ShowItems(true);
	WxBoxSizer35->ShowItems(true);
	WxBoxSizer36->ShowItems(true);
	WxBoxSizer37->ShowItems(true);
	recalcBitVal();

	m_fileConfig->SetPath("/Settings");

	m_fileConfig->Write(wxT("LastUsedProfile"), "");

	m_fileConfig->SetPath("/");
}

/*
 * WxAllOffButtonClick
 */
void BitMakerFrm::WxAllOffButtonClick(wxCommandEvent& event)
{
	WxDecimalEdit->SetValue(wxT("0"));
}

/*
 * WxAllOnButtonClick
 */
void BitMakerFrm::WxAllOnButtonClick(wxCommandEvent& event)
{
	if(WxBit0->IsShown())
	{
		WxBit0->SetFont(boldFont);
		WxBit0->ChangeValue(wxT("1"));
	}

	if(WxBit1->IsShown())
	{
		WxBit1->SetFont(boldFont);
		WxBit1->ChangeValue(wxT("1"));
	}

	if(WxBit2->IsShown())
	{
		WxBit2->SetFont(boldFont);
		WxBit2->ChangeValue(wxT("1"));
	}

	if(WxBit3->IsShown())
	{
		WxBit3->SetFont(boldFont);
		WxBit3->ChangeValue(wxT("1"));
	}

	if(WxBit4->IsShown())
	{
		WxBit4->SetFont(boldFont);
		WxBit4->ChangeValue(wxT("1"));
	}

	if(WxBit5->IsShown())
	{
		WxBit5->SetFont(boldFont);
		WxBit5->ChangeValue(wxT("1"));
	}

	if(WxBit6->IsShown())
	{
		WxBit6->SetFont(boldFont);
		WxBit6->ChangeValue(wxT("1"));
	}

	if(WxBit7->IsShown())
	{
		WxBit7->SetFont(boldFont);
		WxBit7->ChangeValue(wxT("1"));
	}

	if(WxBit8->IsShown())
	{
		WxBit8->SetFont(boldFont);
		WxBit8->ChangeValue(wxT("1"));
	}

	if(WxBit9->IsShown())
	{
		WxBit9->SetFont(boldFont);
		WxBit9->ChangeValue(wxT("1"));
	}

	if(WxBit10->IsShown())
	{
		WxBit10->SetFont(boldFont);
		WxBit10->ChangeValue(wxT("1"));
	}

	if(WxBit11->IsShown())
	{
		WxBit11->SetFont(boldFont);
		WxBit11->ChangeValue(wxT("1"));
	}

	if(WxBit12->IsShown())
	{
		WxBit12->SetFont(boldFont);
		WxBit12->ChangeValue(wxT("1"));
	}

	if(WxBit13->IsShown())
	{
		WxBit13->SetFont(boldFont);
		WxBit13->ChangeValue(wxT("1"));
	}

	if(WxBit14->IsShown())
	{
		WxBit14->SetFont(boldFont);
		WxBit14->ChangeValue(wxT("1"));
	}

	if(WxBit15->IsShown())
	{
		WxBit15->SetFont(boldFont);
		WxBit15->ChangeValue(wxT("1"));
	}

	if(WxBit16->IsShown())
	{
		WxBit16->SetFont(boldFont);
		WxBit16->ChangeValue(wxT("1"));
	}

	if(WxBit17->IsShown())
	{
		WxBit17->SetFont(boldFont);
		WxBit17->ChangeValue(wxT("1"));
	}

	if(WxBit18->IsShown())
	{
		WxBit18->SetFont(boldFont);
		WxBit18->ChangeValue(wxT("1"));
	}

	if(WxBit19->IsShown())
	{
		WxBit19->SetFont(boldFont);
		WxBit19->ChangeValue(wxT("1"));
	}

	if(WxBit20->IsShown())
	{
		WxBit20->SetFont(boldFont);
		WxBit20->ChangeValue(wxT("1"));
	}

	if(WxBit21->IsShown())
	{
		WxBit21->SetFont(boldFont);
		WxBit21->ChangeValue(wxT("1"));
	}

	if(WxBit22->IsShown())
	{
		WxBit22->SetFont(boldFont);
		WxBit22->ChangeValue(wxT("1"));
	}

	if(WxBit23->IsShown())
	{
		WxBit23->SetFont(boldFont);
		WxBit23->ChangeValue(wxT("1"));
	}

	if(WxBit24->IsShown())
	{
		WxBit24->SetFont(boldFont);
		WxBit24->ChangeValue(wxT("1"));
	}

	if(WxBit25->IsShown())
	{
		WxBit25->SetFont(boldFont);
		WxBit25->ChangeValue(wxT("1"));
	}

	if(WxBit26->IsShown())
	{
		WxBit26->SetFont(boldFont);
		WxBit26->ChangeValue(wxT("1"));
	}

	if(WxBit27->IsShown())
	{
		WxBit27->SetFont(boldFont);
		WxBit27->ChangeValue(wxT("1"));
	}

	if(WxBit28->IsShown())
	{
		WxBit28->SetFont(boldFont);
		WxBit28->ChangeValue(wxT("1"));
	}

	if(WxBit29->IsShown())
	{
		WxBit29->SetFont(boldFont);
		WxBit29->ChangeValue(wxT("1"));
	}

	if(WxBit30->IsShown())
	{
		WxBit30->SetFont(boldFont);
		WxBit30->ChangeValue(wxT("1"));
	}

	if(WxBit31->IsShown())
	{
		WxBit31->SetFont(boldFont);
		WxBit31->ChangeValue(wxT("1"));
	}

	recalcBitVal();
}

/*
 * Mnucheckforupdates1203Click
 */
void BitMakerFrm::Mnucheckforupdates1203Click(wxCommandEvent& event)
{
	CheckForUpdates(false);
}


void BitMakerFrm::MnuLoadProfileOnStartupClick(wxCommandEvent& event)
{
	m_fileConfig->SetPath(wxT("/Settings"));

	bool isChecked = WxMenuBar1->IsChecked(ID_MNU_LOADLASTPROFILE);
	m_fileConfig->Write(wxT("LoadLastProfile"), isChecked);

	m_fileConfig->SetPath(wxT("/"));
}

void BitMakerFrm::OnRecentProfileClick(wxCommandEvent& event)
{
	loadProfile(m_fileHistory->GetHistoryFile(event.GetId() - wxID_FILE1));
}


void BitMakerFrm::CheckForUpdates(bool bCheckSilently)
{
	bIsCheckingUpdates = true;
	WxMenuBar1->Enable(ID_MNU_CHECKFORUPDATES_1203, false);
    CheckForUpdatesThread *thread = new CheckForUpdatesThread(this, bCheckSilently);
    thread->Create();
    thread->Run();
}


void BitMakerFrm::OnCheckUpdateThread(wxCommandEvent& event)
{
	WxMenuBar1->Enable(ID_MNU_CHECKFORUPDATES_1203, true);
	bIsCheckingUpdates = false;
	return;
}
