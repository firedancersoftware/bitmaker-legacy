#define _WIN32_WINNT 0x501
#include "Objects/MingW/BitMaker_private.h"
#include "CheckForUpdatesThread.h"
#include <wx/textfile.h>
#include <wx/filesys.h>
#include <wx/fs_inet.h>
#include <wx/txtstrm.h>
#include <wx/url.h>
#include <wx/msgdlg.h>
#include <wx/stream.h>
#include <wx/sstream.h>
#include <wx/protocol/http.h>
#include <wx/tokenzr.h>
DEFINE_EVENT_TYPE(wxEVT_CHECKFORUPDATESTHREAD)
CheckForUpdatesThread::CheckForUpdatesThread(wxEvtHandler* pParent, bool bCheckSilently) : wxThread(wxTHREAD_DETACHED), m_pParent(pParent)
{
	b_CheckSilently = bCheckSilently;
}

void* CheckForUpdatesThread::Entry()
{
	wxCommandEvent evt(wxEVT_CHECKFORUPDATESTHREAD, GetId());

	wxHTTP http;
	http.SetDefaultTimeout(10);

	for(size_t i = 0; i < 15; i++)
	{
		if(!http.Connect(wxT("firedancer-software.com")))
		{
			wxSleep(2);
		}
	}

	wxInputStream * stream = http.GetInputStream(wxT("/versionDB.txt"));
	if((http.GetError() == wxPROTO_NOERR) && stream)
	{
		wxString res;
		wxStringOutputStream out_stream(&res);
		stream->Read(out_stream);


		wxStringTokenizer tokenizer(res, "\n");

		wxString product;
		product << INTERNAL_NAME << "=";

		wxString dbVersion;

		while(tokenizer.HasMoreTokens())
		{
			wxString token = tokenizer.GetNextToken();

			wxString version;
			if(token.StartsWith(product, &version))
			{
				dbVersion = version;
				break;
			}
		}

		if(dbVersion == "")
		{
			if(!b_CheckSilently)
			{
				wxMessageBox("Unable to access the update database.\n Please try again.", "Error", wxICON_ERROR | wxOK);
			}
		}
		else
		{
			if(dbVersion.IsSameAs(PRODUCT_VERSION))
			{
				if(!b_CheckSilently)
				{
					wxMessageBox("No new versions available.", "No New Updates", wxOK);
				}
			}
			else
			{
				wxString output;
				output << PRODUCT_NAME << " " << dbVersion << " is now available to download.\n";
				output << "Would you like to go to the website to download it now?";
				int response = wxMessageBox(output, "Download New Version?", wxYES | wxNO | wxICON_EXCLAMATION);//, wxT("New version available."));

				if(response == wxYES)
				{
					wxLaunchDefaultBrowser(wxT("http://firedancer-software.com/software/bitmaker"));
				}
			}
		}
         wxDELETE(stream);
	}
	else
	{
		if(!b_CheckSilently)
		{
			wxMessageBox("An error occurred accessing the version database.\nPlease try again.", "Version Check Failed", wxICON_ERROR | wxOK);
//		}
/*		switch(http.GetError())
		{
			case wxPROTO_NETERR:
				wxMessageBox("A generic network error occurred.");
				break;
			case wxPROTO_PROTERR:
				wxMessageBox("An error occurred during negotiation.");
				break;
			case wxPROTO_CONNERR:
				wxMessageBox("The client failed to connect the server.");
				break;
			case wxPROTO_INVVAL:
				wxMessageBox("Invalid value.");
				break;
			case wxPROTO_NOHNDLR:
				wxMessageBox("Not currently used.");
				break;
			case wxPROTO_NOFILE:
				wxMessageBox("The remote file doesn't exist.");
				break;
			case wxPROTO_ABRT:
				wxMessageBox("Last action aborted.");
				break;
			case wxPROTO_RCNCT:
				wxMessageBox("An error occurred during reconnection.");
				break;
			case wxPROTO_STREAMING:
				wxMessageBox("Someone tried to send a command during a transfer.");
				break;

		}
*/
	}
}
	http.Close();

	wxSleep(2);

	evt.SetString(wxT("Thread-End"));
	wxPostEvent(m_pParent, evt);

	return 0;
}
