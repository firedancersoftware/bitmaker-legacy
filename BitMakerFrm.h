///-----------------------------------------------------------------
///
/// @file      BitMakerFrm.h
/// @author    Firedancer Software
/// Created:   21/05/2012 8:51:27 AM
/// @section   DESCRIPTION
///            BitMakerFrm class declaration
///
///------------------------------------------------------------------

#ifndef __BITMAKERFRM_H__
#define __BITMAKERFRM_H__

#ifdef __BORLANDC__
	#pragma hdrstop
#endif

#ifndef WX_PRECOMP
	#include <wx/wx.h>
	#include <wx/frame.h>
#else
	#include <wx/wxprec.h>
#endif

//Do not add custom headers between
//Header Include Start and Header Include End.
//wxDev-C++ designer will remove them. Add custom headers after the block.
////Header Include Start
#include <wx/menu.h>
#include <wx/config.h> // Needed For wxFileHistory
#include <wx/docview.h> // Needed For wxFileHistory
#include <wx/button.h>
#include <wx/stattext.h>
#include <wx/textctrl.h>
#include <wx/panel.h>
#include <wx/sizer.h>
#include <wx/fileconf.h>
#include <wx/wfstream.h>
////Header Include End


////Dialog Style Start
#undef BitMakerFrm_STYLE
#define BitMakerFrm_STYLE wxCAPTION | wxRESIZE_BORDER | wxSYSTEM_MENU | wxMINIMIZE_BOX | wxMAXIMIZE_BOX | wxCLOSE_BOX
////Dialog Style End

class BitMakerFrm : public wxFrame
{
	private:
		void Mnuwebsite1007Click(wxCommandEvent& event);
		DECLARE_EVENT_TABLE();

	public:
		BitMakerFrm(wxString defaultProfilePath, wxString defaultConfigPath, wxWindow *parent = NULL, wxWindowID id = 1, const wxString &title = wxT("BitMaker"), const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxDefaultSize, long style = BitMakerFrm_STYLE);
		virtual ~BitMakerFrm();
		void WxCalcButtonClick(wxCommandEvent& event);
		void Mnuexit1009Click(wxCommandEvent& event);
		void Mnuabout1008Click(wxCommandEvent& event);
		void WxExitButtonClick(wxCommandEvent& event);
		void Mnuloadprofile1195Click(wxCommandEvent& event);
		void Mnuunloadprofile1198Click(wxCommandEvent& event);
		void WxAllOffButtonClick(wxCommandEvent& event);
		void WxAllOnButtonClick(wxCommandEvent& event);
		void Mnucheckforupdates1203Click(wxCommandEvent& event);
		void MnuLoadProfileOnStartupClick(wxCommandEvent& event);

		void CheckForUpdates(bool bCheckSilently);
		void OnCheckUpdateThread(wxCommandEvent& event);
		void OnRecentProfileClick(wxCommandEvent &event);
	private:
		//Do not add custom control declarations between
		//GUI Control Declaration Start and GUI Control Declaration End.
		//wxDev-C++ will remove them. Add custom code after the block.
//	wxFileInputStream configStream();
		wxFileConfig *m_fileConfig; // Used to save the file history (can be used for other data too)
		////GUI Control Declaration Start
		wxMenuBar *WxMenuBar1;
		wxFileHistory *m_fileHistory; // the most recently opened files
		wxButton *WxExitButton;
		wxButton *WxAllOffButton;
		wxButton *WxAllOnButton;
		wxBoxSizer *WxBoxSizer5;
		wxPanel *WxPanel6;
		wxTextCtrl *WxDecimalEdit;
		wxBoxSizer *WxBoxSizer4;
		wxPanel *WxPanel5;
		wxStaticText *WxStaticText1;
		wxBoxSizer *WxBoxSizer3;
		wxPanel *WxPanel4;
		wxBoxSizer *WxBoxSizer2;
		wxPanel *WxPanel2;
		wxStaticText *WxBit31Label;
		wxBoxSizer *WxBoxSizer69;
		wxPanel *WxPanel69;
		wxTextCtrl *WxBit31;
		wxBoxSizer *WxBoxSizer37;
		wxPanel *WxPanel39;
		wxStaticText *WxBit23Label;
		wxBoxSizer *WxBoxSizer68;
		wxPanel *WxPanel68;
		wxTextCtrl *WxBit23;
		wxBoxSizer *WxBoxSizer36;
		wxPanel *WxPanel38;
		wxStaticText *WxBit15Label;
		wxBoxSizer *WxBoxSizer67;
		wxPanel *WxPanel67;
		wxTextCtrl *WxBit15;
		wxBoxSizer *WxBoxSizer35;
		wxPanel *WxPanel37;
		wxStaticText *WxBit7Label;
		wxBoxSizer *WxBoxSizer66;
		wxPanel *WxPanel66;
		wxTextCtrl *WxBit7;
		wxBoxSizer *WxBoxSizer34;
		wxPanel *WxPanel36;
		wxStaticText *WxBit30Label;
		wxBoxSizer *WxBoxSizer65;
		wxPanel *WxPanel65;
		wxTextCtrl *WxBit30;
		wxBoxSizer *WxBoxSizer33;
		wxPanel *WxPanel35;
		wxStaticText *WxBit22Label;
		wxBoxSizer *WxBoxSizer64;
		wxPanel *WxPanel64;
		wxTextCtrl *WxBit22;
		wxBoxSizer *WxBoxSizer32;
		wxPanel *WxPanel34;
		wxStaticText *WxBit14Label;
		wxBoxSizer *WxBoxSizer63;
		wxPanel *WxPanel63;
		wxTextCtrl *WxBit14;
		wxBoxSizer *WxBoxSizer31;
		wxPanel *WxPanel33;
		wxStaticText *WxBit6Label;
		wxBoxSizer *WxBoxSizer62;
		wxPanel *WxPanel62;
		wxTextCtrl *WxBit6;
		wxBoxSizer *WxBoxSizer30;
		wxPanel *WxPanel32;
		wxStaticText *WxBit29Label;
		wxBoxSizer *WxBoxSizer61;
		wxPanel *WxPanel61;
		wxTextCtrl *WxBit29;
		wxBoxSizer *WxBoxSizer29;
		wxPanel *WxPanel31;
		wxStaticText *WxBit21Label;
		wxBoxSizer *WxBoxSizer60;
		wxPanel *WxPanel60;
		wxTextCtrl *WxBit21;
		wxBoxSizer *WxBoxSizer28;
		wxPanel *WxPanel30;
		wxStaticText *WxBit13Label;
		wxBoxSizer *WxBoxSizer59;
		wxPanel *WxPanel59;
		wxTextCtrl *WxBit13;
		wxBoxSizer *WxBoxSizer27;
		wxPanel *WxPanel29;
		wxStaticText *WxBit5Label;
		wxBoxSizer *WxBoxSizer58;
		wxPanel *WxPanel58;
		wxTextCtrl *WxBit5;
		wxBoxSizer *WxBoxSizer26;
		wxPanel *WxPanel28;
		wxStaticText *WxBit28Label;
		wxBoxSizer *WxBoxSizer57;
		wxPanel *WxPanel57;
		wxTextCtrl *WxBit28;
		wxBoxSizer *WxBoxSizer25;
		wxPanel *WxPanel27;
		wxStaticText *WxBit20Label;
		wxBoxSizer *WxBoxSizer56;
		wxPanel *WxPanel56;
		wxTextCtrl *WxBit20;
		wxBoxSizer *WxBoxSizer24;
		wxPanel *WxPanel26;
		wxStaticText *WxBit12Label;
		wxBoxSizer *WxBoxSizer55;
		wxPanel *WxPanel55;
		wxTextCtrl *WxBit12;
		wxBoxSizer *WxBoxSizer23;
		wxPanel *WxPanel25;
		wxStaticText *WxBit4Label;
		wxBoxSizer *WxBoxSizer54;
		wxPanel *WxPanel54;
		wxTextCtrl *WxBit4;
		wxBoxSizer *WxBoxSizer22;
		wxPanel *WxPanel24;
		wxStaticText *WxBit27Label;
		wxBoxSizer *WxBoxSizer53;
		wxPanel *WxPanel53;
		wxTextCtrl *WxBit27;
		wxBoxSizer *WxBoxSizer21;
		wxPanel *WxPanel23;
		wxStaticText *WxBit19Label;
		wxBoxSizer *WxBoxSizer52;
		wxPanel *WxPanel52;
		wxTextCtrl *WxBit19;
		wxBoxSizer *WxBoxSizer20;
		wxPanel *WxPanel22;
		wxStaticText *WxBit11Label;
		wxBoxSizer *WxBoxSizer51;
		wxPanel *WxPanel51;
		wxTextCtrl *WxBit11;
		wxBoxSizer *WxBoxSizer19;
		wxPanel *WxPanel21;
		wxStaticText *WxBit3Label;
		wxBoxSizer *WxBoxSizer50;
		wxPanel *WxPanel50;
		wxTextCtrl *WxBit3;
		wxBoxSizer *WxBoxSizer18;
		wxPanel *WxPanel20;
		wxStaticText *WxBit26Label;
		wxBoxSizer *WxBoxSizer49;
		wxPanel *WxPanel49;
		wxTextCtrl *WxBit26;
		wxBoxSizer *WxBoxSizer17;
		wxPanel *WxPanel19;
		wxStaticText *WxBit18Label;
		wxBoxSizer *WxBoxSizer48;
		wxPanel *WxPanel48;
		wxTextCtrl *WxBit18;
		wxBoxSizer *WxBoxSizer16;
		wxPanel *WxPanel18;
		wxStaticText *WxBit10Label;
		wxBoxSizer *WxBoxSizer47;
		wxPanel *WxPanel47;
		wxTextCtrl *WxBit10;
		wxBoxSizer *WxBoxSizer15;
		wxPanel *WxPanel17;
		wxStaticText *WxBit2Label;
		wxBoxSizer *WxBoxSizer46;
		wxPanel *WxPanel46;
		wxTextCtrl *WxBit2;
		wxBoxSizer *WxBoxSizer14;
		wxPanel *WxPanel16;
		wxStaticText *WxBit25Label;
		wxBoxSizer *WxBoxSizer45;
		wxPanel *WxPanel45;
		wxTextCtrl *WxBit25;
		wxBoxSizer *WxBoxSizer13;
		wxPanel *WxPanel15;
		wxStaticText *WxBit17Label;
		wxBoxSizer *WxBoxSizer44;
		wxPanel *WxPanel44;
		wxTextCtrl *WxBit17;
		wxBoxSizer *WxBoxSizer12;
		wxPanel *WxPanel14;
		wxStaticText *WxBit9Label;
		wxBoxSizer *WxBoxSizer43;
		wxPanel *WxPanel43;
		wxTextCtrl *WxBit9;
		wxBoxSizer *WxBoxSizer11;
		wxPanel *WxPanel13;
		wxStaticText *WxBit1Label;
		wxBoxSizer *WxBoxSizer42;
		wxPanel *WxPanel42;
		wxTextCtrl *WxBit1;
		wxBoxSizer *WxBoxSizer10;
		wxPanel *WxPanel12;
		wxStaticText *WxBit24Label;
		wxBoxSizer *WxBoxSizer38;
		wxPanel *WxPanel3;
		wxTextCtrl *WxBit24;
		wxBoxSizer *WxBoxSizer9;
		wxPanel *WxPanel11;
		wxStaticText *WxBit16Label;
		wxBoxSizer *WxBoxSizer41;
		wxPanel *WxPanel41;
		wxTextCtrl *WxBit16;
		wxBoxSizer *WxBoxSizer8;
		wxPanel *WxPanel10;
		wxStaticText *WxBit8Label;
		wxBoxSizer *WxBoxSizer40;
		wxPanel *WxPanel40;
		wxTextCtrl *WxBit8;
		wxBoxSizer *WxBoxSizer7;
		wxPanel *WxPanel9;
		wxStaticText *WxBit0Label;
		wxBoxSizer *WxBoxSizer39;
		wxPanel *WxPanel7;
		wxTextCtrl *WxBit0;
		wxBoxSizer *WxBoxSizer6;
		wxPanel *WxPanel8;
		wxGridSizer *WxGridSizer2;
		wxPanel *WxPanel1;
		wxBoxSizer *WxBoxSizer1;
		////GUI Control Declaration End


	private:
		//Note: if you receive any error with these enum IDs, then you need to
		//change your old form code that are based on the #define control IDs.
		//#defines may replace a numeric value for the enum names.
		//Try copy and pasting the below block in your old form header files.
		enum
		{
			////GUI Enum Control ID Start
			ID_MNU_FILE_1005 = 1005,
			ID_MNU_LOADPROFILE_1195 = 1195,
			ID_MNU_UNLOADPROFILE_1198 = 1198,
			ID_MNU_EXIT_1009 = 1009,
			ID_MNU_HELP_1006 = 1006,
			ID_MNU_CHECKFORUPDATES_1203 = 1203,
			ID_MNU_WEBSITE_1007 = 1007,
			ID_MNU_ABOUT_1008 = 1008,

			ID_WXEXITBUTTON = 1229,
			ID_WXALLOFFBUTTON = 1228,
			ID_WXALLONBUTTON = 1227,
			ID_WXPANEL6 = 1219,
			ID_WXDECIMALEDIT = 1230,
			ID_WXPANEL5 = 1218,
			ID_WXSTATICTEXT1 = 1231,
			ID_WXPANEL4 = 1215,
			ID_WXPANEL2 = 1211,
			ID_WXBIT31LABEL = 1512,
			ID_WXPANEL69 = 1542,
			ID_WXBIT31 = 1514,
			ID_WXPANEL39 = 1352,
			ID_WXBIT23LABEL = 1509,
			ID_WXPANEL68 = 1541,
			ID_WXBIT23 = 1511,
			ID_WXPANEL38 = 1351,
			ID_WXBIT15LABEL = 1506,
			ID_WXPANEL67 = 1540,
			ID_WXBIT15 = 1508,
			ID_WXPANEL37 = 1350,
			ID_WXBIT7LABEL = 1503,
			ID_WXPANEL66 = 1539,
			ID_WXBIT7 = 1505,
			ID_WXPANEL36 = 1349,
			ID_WXBIT30LABEL = 1497,
			ID_WXPANEL65 = 1538,
			ID_WXBIT30 = 1499,
			ID_WXPANEL35 = 1348,
			ID_WXBIT22LABEL = 1494,
			ID_WXPANEL64 = 1537,
			ID_WXBIT22 = 1496,
			ID_WXPANEL34 = 1347,
			ID_WXBIT14LABEL = 1491,
			ID_WXPANEL63 = 1536,
			ID_WXBIT14 = 1493,
			ID_WXPANEL33 = 1346,
			ID_WXBIT6LABEL = 1488,
			ID_WXPANEL62 = 1535,
			ID_WXBIT6 = 1490,
			ID_WXPANEL32 = 1345,
			ID_WXBIT29LABEL = 1481,
			ID_WXPANEL61 = 1534,
			ID_WXBIT29 = 1483,
			ID_WXPANEL31 = 1344,
			ID_WXBIT21LABEL = 1478,
			ID_WXPANEL60 = 1533,
			ID_WXBIT21 = 1480,
			ID_WXPANEL30 = 1343,
			ID_WXBIT13LABEL = 1475,
			ID_WXPANEL59 = 1532,
			ID_WXBIT13 = 1477,
			ID_WXPANEL29 = 1342,
			ID_WXBIT5LABEL = 1472,
			ID_WXPANEL58 = 1531,
			ID_WXBIT5 = 1474,
			ID_WXPANEL28 = 1341,
			ID_WXBIT28LABEL = 1465,
			ID_WXPANEL57 = 1530,
			ID_WXBIT28 = 1467,
			ID_WXPANEL27 = 1315,
			ID_WXBIT20LABEL = 1462,
			ID_WXPANEL56 = 1529,
			ID_WXBIT20 = 1464,
			ID_WXPANEL26 = 1314,
			ID_WXBIT12LABEL = 1459,
			ID_WXPANEL55 = 1528,
			ID_WXBIT12 = 1461,
			ID_WXPANEL25 = 1313,
			ID_WXBIT4LABEL = 1456,
			ID_WXPANEL54 = 1527,
			ID_WXBIT4 = 1458,
			ID_WXPANEL24 = 1312,
			ID_WXBIT27LABEL = 1450,
			ID_WXPANEL53 = 1451,
			ID_WXBIT27 = 1336,
			ID_WXPANEL23 = 1311,
			ID_WXBIT19LABEL = 1448,
			ID_WXPANEL52 = 1449,
			ID_WXBIT19 = 1330,
			ID_WXPANEL22 = 1310,
			ID_WXBIT11LABEL = 1446,
			ID_WXPANEL51 = 1447,
			ID_WXBIT11 = 1324,
			ID_WXPANEL21 = 1309,
			ID_WXBIT3LABEL = 1444,
			ID_WXPANEL50 = 1445,
			ID_WXBIT3 = 1318,
			ID_WXPANEL20 = 1308,
			ID_WXBIT26LABEL = 1437,
			ID_WXPANEL49 = 1526,
			ID_WXBIT26 = 1439,
			ID_WXPANEL19 = 1274,
			ID_WXBIT18LABEL = 1434,
			ID_WXPANEL48 = 1525,
			ID_WXBIT18 = 1436,
			ID_WXPANEL18 = 1273,
			ID_WXBIT10LABEL = 1431,
			ID_WXPANEL47 = 1524,
			ID_WXBIT10 = 1433,
			ID_WXPANEL17 = 1272,
			ID_WXBIT2LABEL = 1428,
			ID_WXPANEL46 = 1523,
			ID_WXBIT2 = 1430,
			ID_WXPANEL16 = 1271,
			ID_WXBIT25LABEL = 1421,
			ID_WXPANEL45 = 1522,
			ID_WXBIT25 = 1423,
			ID_WXPANEL15 = 1246,
			ID_WXBIT17LABEL = 1418,
			ID_WXPANEL44 = 1521,
			ID_WXBIT17 = 1420,
			ID_WXPANEL14 = 1245,
			ID_WXBIT9LABEL = 1415,
			ID_WXPANEL43 = 1520,
			ID_WXBIT9 = 1417,
			ID_WXPANEL13 = 1244,
			ID_WXBIT1LABEL = 1410,
			ID_WXPANEL42 = 1519,
			ID_WXBIT1 = 1412,
			ID_WXPANEL12 = 1243,
			ID_WXBIT24LABEL = 1392,
			ID_WXPANEL3 = 1518,
			ID_WXBIT24 = 1394,
			ID_WXPANEL11 = 1242,
			ID_WXBIT16LABEL = 1407,
			ID_WXPANEL41 = 1517,
			ID_WXBIT16 = 1409,
			ID_WXPANEL10 = 1240,
			ID_WXBIT8LABEL = 1404,
			ID_WXPANEL40 = 1516,
			ID_WXBIT8 = 1406,
			ID_WXPANEL9 = 1238,
			ID_WXBIT0LABEL = 1397,
			ID_WXPANEL7 = 1515,
			ID_WXBIT0 = 1399,
			ID_WXPANEL8 = 1234,
			ID_WXPANEL1 = 1209,
			////GUI Enum Control ID End
			ID_MNU_MENUITEM11_1543 = 1543,
			ID_MNU_LOADLASTPROFILE = 1544,
			ID_DUMMY_VALUE_ //don't remove this value unless you have other enum values
		};

	private:
		void OnClose(wxCloseEvent& event);
		void OnClickBit(wxMouseEvent& event);
		void CreateGUIControls();
		void recalcBitVal();
		void resetBitLabels();
		void loadProfile(wxString filePath, bool isStartup = false);
		void removeFromHistory(wxString filePath);

		bool bIsCheckingUpdates;

		wxString profilePath;
		wxString configPath;

		wxFont normalFont;
		wxFont boldFont;

};

#endif
